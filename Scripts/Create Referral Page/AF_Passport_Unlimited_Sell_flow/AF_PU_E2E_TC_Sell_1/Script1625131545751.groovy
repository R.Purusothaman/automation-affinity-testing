import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Test Cases/Common TestCase/AF_Create_Randome_Name'), null)

WebUI.callTestCase(findTestCase('Test Cases/Common TestCase/AF_Naviagte_to_Passport_Unlimited_URL'), null)

WebUI.verifyElementPresent(findTestObject('Object Repository/USBank_Create_Referral_Page/text_USBank_You_are_interested'), GlobalVariable.timeout)

WebUI.verifyElementPresent(findTestObject('Object Repository/USBank_Create_Referral_Page/text_USBank_Choose_All'),GlobalVariable.timeout)

WebUI.verifyElementPresent(findTestObject('USBank_Create_Referral_Page/section_USBank_Progressbar'), GlobalVariable.timeout)

progressValue = WebUI.getAttribute(findTestObject('USBank_Create_Referral_Page/section_USBank_Progressbar'), 'aria-valuenow')

System.out.println("The current progress is: "+progressValue)

WebUI.click(findTestObject('Object Repository/USBank_Create_Referral_Page/button_USBank_SellBtn'))

WebUI.click(findTestObject('Object Repository/USBank_Create_Referral_Page/button_USBank_Next_ReferlType'))

WebUI.verifyElementPresent(findTestObject('Object Repository/USBank_Personal_Info_Page/text_USBank_Personal_Info'), GlobalVariable.timeout)

progressValue = WebUI.getAttribute(findTestObject('USBank_Create_Referral_Page/section_USBank_Progressbar'), 'aria-valuenow')

System.out.println("The current progress is: "+progressValue)

WebUI.verifyElementPresent(findTestObject('Object Repository/USBank_Personal_Info_Page/input_USBank_FirstName'), GlobalVariable.timeout)

WebUI.setText(findTestObject('Object Repository/USBank_Personal_Info_Page/input_USBank_FirstName'), findTestData('Create_Referral').getValue(1, 1))

WebUI.verifyElementPresent(findTestObject('Object Repository/USBank_Personal_Info_Page/input_USBank_LastName'), GlobalVariable.timeout)

WebUI.setText(findTestObject('Object Repository/USBank_Personal_Info_Page/input_USBank_LastName'), GlobalVariable.RandomName)

WebUI.verifyElementPresent(findTestObject('Object Repository/USBank_Personal_Info_Page/input_USBank_Email'), GlobalVariable.timeout)

WebUI.setText(findTestObject('Object Repository/USBank_Personal_Info_Page/input_USBank_Email'), findTestData('Create_Referral').getValue(3, 1))

WebUI.verifyElementPresent(findTestObject('Object Repository/USBank_Personal_Info_Page/input_USBank_PrimaryNumber'), GlobalVariable.timeout)

WebUI.setText(findTestObject('Object Repository/USBank_Personal_Info_Page/input_USBank_PrimaryNumber'), findTestData('Create_Referral').getValue(4, 1))

WebUI.click(findTestObject('Object Repository/USBank_Personal_Info_Page/button_USBank_Phone'))

WebUI.click(findTestObject('Object Repository/USBank_Personal_Info_Page/button_USBank_Mobile'))

WebUI.verifyElementPresent(findTestObject('Object Repository/USBank_Personal_Info_Page/checkbox_USBank_AgreeCheckbox'), GlobalVariable.timeout)

WebUI.verifyElementPresent(findTestObject('Object Repository/USBank_Personal_Info_Page/text_USBank_AgreeStatement'), GlobalVariable.timeout)

WebUI.click(findTestObject('Object Repository/USBank_Personal_Info_Page/checkbox_USBank_AgreeCheckbox'))

WebUI.click(findTestObject('USBank_Create_Referral_Page/button_USBank_Next_InfoScn'))

progressValue = WebUI.getAttribute(findTestObject('USBank_Create_Referral_Page/section_USBank_Progressbar'), 'aria-valuenow')

System.out.println("The current progress is: "+progressValue)

WebUI.verifyElementPresent(findTestObject('Object Repository/USBank_Personal_Info_Page/text_USBank_SellLocationHeader'), GlobalVariable.timeout)

WebUI.click(findTestObject('Object Repository/USBank_Create_Referral_Page/button_USBank_PrevBtn_SellLoc'))

WebUI.click(findTestObject('USBank_Create_Referral_Page/button_USBank_PrevBtn_InfoScn'))

WebUI.click(findTestObject('Object Repository/USBank_Create_Referral_Page/button_USBank_Next_ReferlType'))

WebUI.click(findTestObject('Object Repository/USBank_Create_Referral_Page/button_USBank_Next_InfoScn'))

WebUI.verifyElementPresent(findTestObject('Object Repository/USBank_Personal_Info_Page/text_USBank_SellLocationHeader'), GlobalVariable.timeout)

value = findTestData('Create_Referral').getValue(5, 8)

ElementFinder = findTestData('Create_Referral').getValue(6, 2)

length = value.length()

CustomKeywords.'com.Utilities.Newfunction.slowtype'(ElementFinder, value, length)

WebUI.click(findTestObject('Object Repository/USBank_Personal_Info_Page/DrpDown_USBank_Danbury_Ct,Danbury,CT,USA'))

WebUI.setText(findTestObject('Object Repository/USBank_Personal_Info_Page/input_USBank_APIUnit'), findTestData('Create_Referral').getValue(8, 1))

WebUI.click(findTestObject('Object Repository/USBank_Create_Referral_Page/button_USBank_Next_SellLoc'))

WebUI.waitForElementVisible(findTestObject('Object Repository/USBank_Personal_Info_Page/text_USBank_SubmitRequest'), 1)

WebUI.verifyElementPresent(findTestObject('Object Repository/USBank_Personal_Info_Page/text_USBank_ThankYouPageText'), GlobalVariable.timeout)

WebUI.verifyElementPresent(findTestObject('Object Repository/USBank_Personal_Info_Page/text_USBank_ThankYou_AgentReachOutMsg'), GlobalVariable.timeout)

WebUI.verifyElementPresent(findTestObject('Object Repository/USBank_Personal_Info_Page/text_USBank_DisclaimerMsg'), GlobalVariable.timeout)

WebUI.click(findTestObject('Object Repository/USBank_Personal_Info_Page/button_USBank_ThankYou_Success_CloseBtn'))

WebUI.closeBrowser()

WebUI.delay(60)

WebUI.callTestCase(findTestCase('Test Cases/DB Validation/HEM_DB_Validation/HEM_Passport_Unlimited_Validation/HEM_PU_DB_Sell_TC_1'), null)






