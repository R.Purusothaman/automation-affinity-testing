import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Test Cases/Common TestCase/AF_Create_Randome_Name'), null)

WebUI.callTestCase(findTestCase('Test Cases/Common TestCase/AF_Naviagte_to_Passport_Unlimited_URL'), null)

WebUI.verifyElementPresent(findTestObject('Object Repository/USBank_Create_Referral_Page/text_USBank_You_are_interested'), GlobalVariable.timeout)

WebUI.verifyElementPresent(findTestObject('Object Repository/USBank_Create_Referral_Page/text_USBank_Choose_All'),GlobalVariable.timeout)

WebUI.verifyElementPresent(findTestObject('USBank_Create_Referral_Page/section_USBank_Progressbar'), GlobalVariable.timeout)

progressValue = WebUI.getAttribute(findTestObject('USBank_Create_Referral_Page/section_USBank_Progressbar'), 'aria-valuenow')

System.out.println("The current progress is: "+progressValue)

WebUI.click(findTestObject('Object Repository/USBank_Create_Referral_Page/button_USBank_BuyBtn'))

WebUI.click(findTestObject('Object Repository/USBank_Create_Referral_Page/button_USBank_Next_ReferlType'))

WebUI.verifyElementPresent(findTestObject('Object Repository/USBank_Personal_Info_Page/text_USBank_Personal_Info'), GlobalVariable.timeout)

progressValue = WebUI.getAttribute(findTestObject('USBank_Create_Referral_Page/section_USBank_Progressbar'), 'aria-valuenow')

System.out.println("The current progress is: "+progressValue)

WebUI.verifyElementPresent(findTestObject('Object Repository/USBank_Personal_Info_Page/input_USBank_Email'), GlobalVariable.timeout)

WebUI.setText(findTestObject('Object Repository/USBank_Personal_Info_Page/input_USBank_Email'), findTestData('Create_Referral').getValue(3, 1))

WebUI.clearText(findTestObject('Object Repository/USBank_Personal_Info_Page/input_USBank_Email'))

WebUI.click(findTestObject('Object Repository/USBank_Personal_Info_Page/input_USBank_LastName'))

WebUI.verifyElementPresent(findTestObject('Object Repository/USBank_Personal_Info_Page/input_USBank_Email'), GlobalVariable.timeout)

WebUI.setText(findTestObject('Object Repository/USBank_Personal_Info_Page/input_USBank_Email'), findTestData('Create_Referral').getValue(3, 2))

WebUI.click(findTestObject('Object Repository/USBank_Personal_Info_Page/input_USBank_LastName'))

ErrMsgFname = WebUI.getText(findTestObject('Object Repository/USBank_Personal_Info_Page/text_USBank_ErrMsgEmail'))

System.out.println(ErrMsgFname)

WebUI.verifyEqual(ErrMsgFname, "Please enter a valid email address")

WebUI.clearText(findTestObject('Object Repository/USBank_Personal_Info_Page/input_USBank_Email'))

WebUI.verifyElementPresent(findTestObject('Object Repository/USBank_Personal_Info_Page/input_USBank_Email'), GlobalVariable.timeout)

WebUI.setText(findTestObject('Object Repository/USBank_Personal_Info_Page/input_USBank_Email'), findTestData('Create_Referral').getValue(3, 3))

WebUI.click(findTestObject('Object Repository/USBank_Personal_Info_Page/input_USBank_LastName'))

ErrMsgFname = WebUI.getText(findTestObject('Object Repository/USBank_Personal_Info_Page/text_USBank_ErrMsgEmail'))

System.out.println(ErrMsgFname)

WebUI.verifyEqual(ErrMsgFname, "Please enter a valid email address")

WebUI.verifyElementHasAttribute(findTestObject('USBank_Create_Referral_Page/button_USBank_Next_InfoScn'), 'disabled', GlobalVariable.timeout)

WebUI.closeBrowser()
