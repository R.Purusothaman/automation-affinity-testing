import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.sql.*

import com.kms.katalon.core.testobject.ConditionType as ConditionType

import com.kms.katalon.core.testobject.TestObjectProperty as TestObjectProperty

import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS

import groovy.json.JsonSlurper as JsonSlurper

import internal.GlobalVariable as GlobalVariable

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile

import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import static com.kms.katalon.core.testdata.TestDataFactory.findTestData

import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint

import com.kms.katalon.core.model.FailureHandling as FailureHandling

import com.kms.katalon.core.testcase.TestCase as TestCase

import com.kms.katalon.core.testdata.TestData as TestData

import com.kms.katalon.core.testobject.TestObject as TestObject

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint

import com.kms.katalon.core.webservice.verification.WSResponseManager as WSResponseManager

import com.kms.katalon.core.testobject.RequestObject as RequestObject

import frameworkFunctions.Log as Log

 

//Initial Steps

String sServiceName = 'GetReferral'

String sCurrentDateTime = CustomKeywords.'frameworkFunctions.UtilityLib.CurrentDateTime_UF'()

//Responses stored

String sResponseFileName = (((((GlobalVariable.sJSONResultPath + '/Response_') + GlobalVariable.sTestCaseName) + '_') +

sServiceName) + sCurrentDateTime) + '.json'

KeywordUtil.logInfo('Response is stored in ' + sResponseFileName)

 

 

HashMap<String, String> hmDBValues = new HashMap<String, String>()

//Getting DB values

//String RandomLastName = 'zohef'

String RandomLastName = GlobalVariable.RandomName

String sdbColumnNames = sDBColumNames

String sdbQuery = sdbQuery

String sQuery = sdbQuery.replace('lastname', RandomLastName)

System.out.println("The query is: "+sQuery)

hmDBValues = CustomKeywords.'frameworkFunctions.DataExtraction.getDBValues'(sdbColumnNames, sQuery)



println hmDBValues

String FirstName = hmDBValues.get('RCT.firstName')

System.out.println(FirstName+"\n")

String LastName = hmDBValues.get('RCT.lastName')

System.out.println(LastName+"\n")

String Email = hmDBValues.get('RCT.primaryEmail')

String Mobile = hmDBValues.get('RCT.primaryMobileNo')

String ReferralType = hmDBValues.get('RST.service_type')

String City = hmDBValues.get('RST.prop_address_city_name')

String State = hmDBValues.get('RST.prop_address_state_name')

String PropertyType = hmDBValues.get('RST.property_type')

String OwnershipType = hmDBValues.get('RST.ownership_type')

String AddlComments = hmDBValues.get('RST.additional_comments')

String status = hmDBValues.get('RMT.status')

String created_Channel = hmDBValues.get('RMT.created_channel')

String hem_case_id = hmDBValues.get('RMT.hem_case_id')


System.out.println(FirstName+"\n"+LastName+"\n"+Email+"\n"+Mobile+"\n"+ReferralType+"\n"+City+"\n"+State+"\n"+PropertyType+"\n"+AddlComments+"\n"+status+"\n"+created_Channel+"\n"+hem_case_id)



WebUI.verifyEqual(FirstName, 'TestFirstName')

WebUI.verifyEqual(LastName, RandomLastName)

WebUI.verifyEqual(Email, 'TestPractice@gmail.com')

WebUI.verifyEqual(Mobile, '123-456-7890')

WebUI.verifyEqual(ReferralType, 'selling')

WebUI.verifyEqual(City, 'Danbury')

WebUI.verifyEqual(State, 'CT')

WebUI.verifyEqual(GlobalVariable.hem_case_id, hem_case_id)

//WebUI.verifyEqual(PropertyType, '[1]')
//
//WebUI.verifyEqual(OwnershipType, '[4]')
//
//WebUI.verifyEqual(AddlComments, 'test')

WebUI.verifyEqual(status, 'Open')

WebUI.verifyEqual(created_Channel, 'US Bank')

//Send and get the response

/*try {

			  //Send Request Body

   def sRequest = ((findTestObject('Object Repository/Cartus/GetReferal')) as RequestObject)

 

  //Build the url

			  String sUrl = GlobalVariable.sGetEndPoint

 

			  String sReferral = sReferralID

 

  

			  sUrl = sUrl + sReferral

			 

			  println sUrl

			 

							 sRequest.setRestUrl(sUrl)

 

			  //Make POST request

			  def sResponseData = WS.sendRequest(sRequest)

			  String sResponseText = sResponseData.getResponseText()

			 

 

			  //Writing to JSON File

CustomKeywords.'frameworkFunctions.processJSON.writeToFile'(sResponseFileName, sResponseText)

 

			  //Verify Response Status

			  WS.verifyResponseStatusCode(sResponseData, 200)

			 

  

			  }

			  catch(Exception e)

			  {

							

			  }

			 

finally {

   // CustomKeywords.'frameworkFunctions.DataExtraction.closeDatabaseConnection'()

}*/