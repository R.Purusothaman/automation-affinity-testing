import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import java.sql.*

import com.kms.katalon.core.testobject.ConditionType as ConditionType

import com.kms.katalon.core.testobject.TestObjectProperty as TestObjectProperty

import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS

import groovy.json.JsonSlurper as JsonSlurper

import internal.GlobalVariable as GlobalVariable

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile

import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW

import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import static com.kms.katalon.core.testdata.TestDataFactory.findTestData

import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint

import com.kms.katalon.core.model.FailureHandling as FailureHandling

import com.kms.katalon.core.testcase.TestCase as TestCase

import com.kms.katalon.core.testdata.TestData as TestData

import com.kms.katalon.core.testobject.TestObject as TestObject

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint

import com.kms.katalon.core.webservice.verification.WSResponseManager as WSResponseManager

import com.kms.katalon.core.testobject.RequestObject as RequestObject

import frameworkFunctions.Log as Log

 

//Initial Steps

String sServiceName = 'GetReferral'

String sCurrentDateTime = CustomKeywords.'frameworkFunctions.UtilityLib.CurrentDateTime_UF'()

//Responses stored

String sResponseFileName = (((((GlobalVariable.sJSONResultPath + '/Response_') + GlobalVariable.sTestCaseName) + '_') +

sServiceName) + sCurrentDateTime) + '.json'

KeywordUtil.logInfo('Response is stored in ' + sResponseFileName)

 

 

HashMap<String, String> hmDBValues = new HashMap<String, String>()

//Getting DB values

//String RandomLastName = 'wnxjz'

String RandomLastName = GlobalVariable.RandomName

RandomLastName = RandomLastName.capitalize()

String sdbColumnNames = sDBColumNames

String sdbQuery = sdbQuery

String sQuery = sdbQuery.replace('lastname', RandomLastName)

hmDBValues = CustomKeywords.'frameworkFunctions.DataExtraction.getHEMDBValues'(sdbColumnNames, sQuery)

println hmDBValues


String HESHoldCaseId = hmDBValues.get('HESHoldCaseId')

String Clinbr = hmDBValues.get('CliNbr')

String FirstName = hmDBValues.get('Fname')

String LastName = hmDBValues.get('Lname')

String emailAddress =  hmDBValues.get('EmailAdd')

String phone = hmDBValues.get('TxtPhone')

String ocphone = hmDBValues.get('OCPhone')

String ohphone = hmDBValues.get('OHPhone')

String Checkbox = hmDBValues.get('TxtOptInFlg')

String Leadsource = hmDBValues.get('LeadSource')

String buyLocation = hmDBValues.get('BuyLocation')

String sellLocation = hmDBValues.get('SellLocation')

String apiunit = hmDBValues.get('OHAddrLine2')

System.out.println(HESHoldCaseId+"\n"+Clinbr+"\n"+FirstName+"\n"+LastName+"\n"+emailAddress+"\n"+phone+"\n"+Checkbox+"\n"+Leadsource+buyLocation+"\n"+sellLocation+"\n")


WebUI.verifyEqual(Clinbr,findTestData('Create_Referral').getValue(12, 10))

WebUI.verifyEqual(FirstName, 'Testfirstname')

WebUI.verifyEqual(LastName, RandomLastName)

WebUI.verifyEqual(emailAddress, 'TestPractice@gmail.com')

WebUI.verifyEqual(phone, null)

WebUI.verifyEqual(ocphone, '123-456-7890')

WebUI.verifyEqual(ohphone,null)

WebUI.verifyEqual(Leadsource,findTestData('Create_Referral').getValue(11, 10))

WebUI.verifyEqual(Checkbox, 'false')

WebUI.verifyEqual(buyLocation, 'Danbury, CT')

WebUI.verifyEqual(sellLocation, 'Danbury, CT')

WebUI.verifyEqual(apiunit, '123456?89$')




//Send and get the response

/*try {

			  //Send Request Body

   def sRequest = ((findTestObject('Object Repository/Cartus/GetReferal')) as RequestObject)

 

  //Build the url

			  String sUrl = GlobalVariable.sGetEndPoint

 

			  String sReferral = sReferralID

 

  

			  sUrl = sUrl + sReferral

			 

			  println sUrl

			 

							 sRequest.setRestUrl(sUrl)

 

			  //Make POST request

			  def sResponseData = WS.sendRequest(sRequest)

			  String sResponseText = sResponseData.getResponseText()

			 

 

			  //Writing to JSON File

CustomKeywords.'frameworkFunctions.processJSON.writeToFile'(sResponseFileName, sResponseText)

 

			  //Verify Response Status

			  WS.verifyResponseStatusCode(sResponseData, 200)

			 

  

			  }

			  catch(Exception e)

			  {

							

			  }

			 

finally {

   // CustomKeywords.'frameworkFunctions.DataExtraction.closeDatabaseConnection'()

}*/