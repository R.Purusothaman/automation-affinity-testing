package com.Utilities

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import java.util.concurrent.TimeUnit

import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.webui.driver.DriverFactory


import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

public class Newfunction {

	@Keyword
	def slowtype(def ElementFinder, def value, def length) {

		WebDriver webDriver = DriverFactory.getWebDriver()

		WebUI.click(findTestObject(ElementFinder))

		for (int i = 0; i < length; i++) {

			WebUI.sendKeys(findTestObject(ElementFinder), value[i])

			webDriver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS)
		}
	}

	@Keyword
	def login_page() {

		WebUI.waitForElementPresent(findTestObject('Page_CB Login Page/text_Sign In'), GlobalVariable.timeout)

		WebUI.setEncryptedText(findTestObject('Object Repository/Page_CB Login Page/input_Password_password'), GlobalVariable.password)

		System.out.println('Sent the password')
	}


	@Keyword
	def ClickListofElements(def xpath4elements, def tagname, def previousstepxpath) {

		WebDriver webDriver = DriverFactory.getWebDriver()

		List<WebElement> elements = webDriver.findElements(By.xpath(xpath4elements))

		int rows = elements.size()

		if(tagname != '') {
			for(int i=0; i < rows ; i++) {

				WebUI.click(findTestObject(previousstepxpath))

				elements.get(i).findElement(By.tagName(tagname)).click()

				System.out.println("Clicked on the dropdown of value "+i)
			}
		}
		else {
			for(int i=0; i < rows ; i++) {

				elements.get(i).click()

				System.out.println("Clicked on the dropdown of value "+i)
			}
		}
	}

	@Keyword
	List <WebElement> getListofElements(def xpath4elements, def tagname) {

		WebDriver webDriver = DriverFactory.getWebDriver()

		List<WebElement> elements = webDriver.findElements(By.xpath(xpath4elements))

		int rows = elements.size()

		WebElement element_row

		System.out.println("The rows is "+rows)

		System.out.println("the tagname is "+tagname)

		if(tagname != '') {
			for(int i=0; i < rows ; i++) {

				element_row = elements.get(i).findElement(By.tagName(tagname))

				String celltext = element_row.getText()

				System.out.println(celltext)
			}
		}
		else {
			for(int i=0; i < rows ; i++) {

				element_row = elements.get(i)

				String celltext = element_row.getText()

				System.out.println(celltext)
			}
		}

		return elements
	}

	@Keyword
	String chars = "abcdefghijklmnopqrstuvwxyz0123456789"

	public static String randomNameGenerator(String chars, int length) {
		Random rand = new Random()
		StringBuilder sb = new StringBuilder()
		for(int i=0; i<length; i++) {
			sb.append(chars.charAt(rand.nextInt(chars.length())))
		}
		return sb.toString()
	}
}
