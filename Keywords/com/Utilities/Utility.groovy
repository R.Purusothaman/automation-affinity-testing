package com.Utilities

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable
import java.text.DateFormat
import java.text.SimpleDateFormat
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import java.util.ArrayList;
import java.util.Collection;
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.webui.driver.DriverFactory as DF

public class Utility {

	Log log = new Log();
	//This function verify the file id downloaded or not and then delete that file
	@Keyword
	public Boolean verifyDownload(String exportFileName) throws Exception {
		String fileName = exportFileName;
		String home = System.getProperty("user.home");
		String filePath = home + File.separator + "Downloads";
		try{
			final File dir = new File(filePath);
			for(int i= 1;i<=60;i++)
			{
				Thread.sleep(1000)
				File[] dirContents = dir.listFiles();
				for (File dirContent : dirContents) {
					if (dirContent.getName().contains(fileName)) {
						dirContent.delete();
						return true;

					}
				}
			}
		}catch(Exception e) {
			return false;
		}
		return false;
	}





	//This function get the windowHandles and verifies if the window is opened
	@Keyword
	public Boolean checkWindowTitle(String windowTitle) throws Exception {
		try
		{
			WebDriver driver = DF.getWebDriver();

			for (String shandle : driver.getWindowHandles()) {
				if (driver.switchTo().window(shandle).getTitle().contains(windowTitle) == true) {
					driver.switchTo().defaultContent();

					return true;
				} else {

					return false;
				}
			}


			return false;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			log.error(e.getMessage());


		}
	}


	//This function returns a date in string format with offset of x days from current date
	@Keyword
	public String getFutureDate(int intDays) {
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Calendar cal = Calendar.getInstance();
		try
		{
			cal.add(Calendar.DATE, intDays);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return dateFormat.format(cal.getTime());
	}

	//This function returns a date in string format with offset of x days from current date
	@Keyword
	public String getBeforeDate(int intDays){

		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Calendar cal = Calendar.getInstance();
		try
		{
			cal.add(Calendar.DATE, -intDays);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return dateFormat.format(cal.getTime());
	}

	//This function gets the current date and return date in string format
	@Keyword
	public String getCurrentDate() {

		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		Date date = new Date();
		String sDate;
		try
		{
			sDate = formatter.format(date);
		}

		catch(Exception e)
		{
			e.printStackTrace();
			log.error(e.getMessage());
		}

		return sDate;
	}

	//This function gets No of length and return random string of that length
	@Keyword
	public String getSaltString(int intlength) {
		String ssaltStr = ""
		try
		{
			int leftLimit = 65; // letter 'A'
			int rightLimit = 90; // letter 'Z'
			StringBuilder ssalt = new StringBuilder();
			Random rnd = new Random();
			while (ssalt.length() < intlength) {
				int randomLimitedInt = leftLimit + (int) (rnd.nextFloat() * (rightLimit - leftLimit + 1));
				ssalt.append((char) randomLimitedInt);
			}
			ssaltStr = ssalt.toString();
		}

		catch(Exception e)
		{
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return ssaltStr;
	}
	//This function gets No of length and return random Number of that length
	@Keyword

	public  String getSaltNumber(int length) {
		StringBuilder ssalt = new StringBuilder();
		try
		{


			int intleftLimit = 49; // number '1'
			int intrightLimit = 57; // number '9'
			Random rnd = new Random();
			for (int i = 0; i < length; i++)
			{
				ssalt.append((char) (intleftLimit + rnd.nextDouble() * (intrightLimit - intleftLimit)));
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return ssalt.toString();
	}
	@Keyword
	public void putData(String sPropertyName, String sValue) {
		Properties prop = new Properties();
		OutputStream output = null;
		def sPropPath = RunConfiguration.getProjectDir() + "\\Properties\\DataValues.properties"
		try
		{
			File filePropPath = new File(sPropPath);
			if(!filePropPath.exists())
			{
				filePropPath.createNewFile();


			}
			output = new FileOutputStream(sPropPath);
			prop.put(sPropertyName, sValue);
			prop.store(output, null);
			output.close();

		}

		catch(Exception e)
		{
			e.printStackTrace();
			log.error(e.getMessage());
		}
	}
	@Keyword
	public ArrayList<String> getTextList(List<WebElement> list) {
		final ArrayList<String> al = new ArrayList<>();
		for (int i = 0; i < list.size(); i++) {
			al.add(list.get(i).getText());
		}
		//		log.info(al.removeAll(Arrays.asList(null, "")));
		return al;
	}
	@Keyword
	public String getAttributeList(List<WebElement> list,String attribute) {
		String attributeValue="";
		for (int i = 0; i < list.size(); i++) {
			attributeValue=list.get(i).getAttribute(attribute);
			log.info("attributeValue is "+ attributeValue)
		}
		//		log.info(al.removeAll(Arrays.asList(null, "")));
		return attributeValue;
	}

	@Keyword
	public void clickSelectedElementInList(String searchComponent, List<WebElement> list) throws Exception {
		for (int i = 0; i < list.size(); i++) {
			String elementListsText = list.get(i).getText();
			System.out.println("ElementListsText is " + elementListsText);
			if (elementListsText.equals(searchComponent)) {
				log.pass(elementListsText + " is selected");
				list.get(i).click();
				break;
			}
		}
	}
	@Keyword
	public void getAttributeAndEnterText(List<WebElement> elements, String Attribute, String text, String inputString) {
		for (int i = 0; i < elements.size(); i++) {
			String attributeText = elements.get(i).getAttribute(Attribute);
			if (attributeText.equals(text)) {
				elements.get(i).clear();
				elements.get(i).sendKeys(inputString);
				log.pass(inputString + " is entered to " + text);
			}
		}
	}

	@Keyword
	public String getData(String sPropertyName) throws Exception

	{
		String sValue = "";
		try {
			def sPropPath = RunConfiguration.getProjectDir() + "\\Properties\\DataValues.properties"
			FileInputStream f = new FileInputStream(sPropPath)
			Properties p = new Properties();
			p.load(f);
			sValue = p.getProperty(sPropertyName);
		}

		catch (Exception e)

		{
			e.printStackTrace();
			log.error(e.getMessage());
		}

		return sValue;
	}

	@Keyword
	public boolean isFilePresent(String sDownloadPath, String sFileName) {
		try {
			final File dir = new File(sDownloadPath);
			final File[] dirContents = dir.listFiles();
			for (final File dirContent : dirContents) {
				if (dirContent.getName().equals(sFileName)) {
					dirContent.delete();
					return true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return false;
	}
	@Keyword
	public Boolean verifyListIsSorted(List<WebElement> list) {

		ArrayList<String> obtainedList = new ArrayList<>();
		for (WebElement we : list) {
			obtainedList.add(we.getText());
		}
		ArrayList<String> sortedList = new ArrayList<>();
		for (String s : obtainedList) {
			sortedList.add(s);
		}
		Collections.sort(sortedList);
		if (sortedList.equals(obtainedList))
			return true;
		else
			return false;

	}
	@Keyword
	// Method to verify the list of elements is displayed or nt
	public void verifyElementsPresent(List<WebElement> elements) throws Exception {
		// looping through the list of links
		for (int i = 0; i < elements.size(); i++) {
			// verify whether element displayed or not
			if (elements.get(i).isDisplayed()) {
				log.pass(elements.get(i).getText() + " is displayed..!!");
			} else {
				log.fail(elements.get(i).getText() + " is not displayed...Please check!!");
			}
		}
	}
}
