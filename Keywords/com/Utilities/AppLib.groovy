package com.Utilities

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable
import org.openqa.selenium.WebDriver
import org.openqa.selenium.support.ui.WebDriverWait

public class AppLib {

	@Keyword
	public void waitForPDFExportLoad() {
		WebDriver driver = DriverFactory.getWebDriver()
		WebDriverWait wait = new WebDriverWait(driver,GlobalVariable.intWaitTime);
		wait.until(ExpectedConditions.invisibilityOfAllElements(driver.findElements
				(By.xpath("(//div[@class='k-loading-pdf-progress k-widget k-progressbar k-progressbar-horizontal'])[2]"))));
	}

	@Keyword
	public String getListingID(String sSuccessMsg) {
		int intStartPosn = sSuccessMsg.indexOf(":") + 1
		int intEndPosn = sSuccessMsg.length() - 1
		String sListingID = sSuccessMsg.substring(intStartPosn, intEndPosn)
		sListingID = sListingID.trim()
		return sListingID
	}
}
