package com.Utilities

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.util.KeywordUtil;

import internal.GlobalVariable

public class Log {

	KeywordLogger log = new KeywordLogger();

	//Logging information
	public void info(String message) throws IOException {

		log.logInfo(message);
		KeywordUtil.logInfo(message);
	}
	//Logging warning
	public void warning(String message) throws Exception {

		log.logWarning(message);
		KeywordUtil.markWarning(message);


	}

	//Logging error
	public void error(String message) throws Exception {

		log.logError(message);
		KeywordUtil.markErrorAndStop(message);



	}

	//Logging fatal error
	public void fatal(String message) throws Exception {

		log.logError(message);
		KeywordUtil.markFailedAndStop(message);


	}
	//Logging pass
	public void pass(String message) throws Exception {

		log.logInfo(message);
		KeywordUtil.markPassed(message);



	}

	//Logging fail
	public void fail(String message) throws Exception {

		log.logError(message);
		KeywordUtil.markPassed(message);


	}
}
