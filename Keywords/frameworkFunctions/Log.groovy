package frameworkFunctions

import java.io.IOException;

import javax.swing.JLabel;
import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.util.KeywordUtil;


public class Log {

	KeywordLogger log = new KeywordLogger();

	//Logging information
	public void info(String message) throws IOException {


		KeywordUtil.logInfo(message);
	}
	//Logging warning
	public void warning(String message) throws Exception {


		KeywordUtil.markWarning(message);


	}

	//Logging error
	public void error(String message) throws Exception {


		KeywordUtil.markErrorAndStop(message);



	}

	//Logging fatal error
	public void fatal(String message) throws Exception {


		KeywordUtil.markFailedAndStop(message);


	}
	//Logging pass
	public void pass(String message) throws Exception {


		KeywordUtil.markPassed(message);



	}

	//Logging fail
	public void fail(String message) throws Exception {


		KeywordUtil.markPassed(message);


	}





}
