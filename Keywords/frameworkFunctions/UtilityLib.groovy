package frameworkFunctions

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.configuration.RunConfiguration
import internal.GlobalVariable
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat


public class UtilityLib {

	@Keyword
	public String CurrentDateTime_UF() {

		String sDate = "";
		//Getting the current date and formatting
		try
		{
			Date dNow = new Date();
			SimpleDateFormat dtSimple = new SimpleDateFormat("dd_MMM_yyyy_HH_mm_ss")

			sDate = dtSimple.format(dNow)

		}

		catch(Exception e)
		{
			e.printStackTrace()
		}

		return sDate;


	}

	//Create a folder for Requests and Responses
	@Keyword
	public String CreateFolder_UF()

	{
		String sResultPath = "";//Initializing ResultPath
		//Creating Results,Log parent folder
		String sCurrentDateTime = CurrentDateTime_UF() //Initialize for Current Date time
		String sRootPath = RunConfiguration.getProjectDir()//Get Current Project root dir

		String sFolderPath = "Responses/"+"Responses_" +  sCurrentDateTime
		sResultPath = sRootPath + "/"+ sFolderPath
		GlobalVariable.sJSONResultPath = sResultPath

		File Resultdir = new File(sResultPath);
		//Checking existence of Results folder

		if(!Resultdir.exists())
		{

			try
			{
				boolean b = Resultdir.mkdir()

			}

			catch(Exception e)
			{
				e.printStackTrace()
			}

		}


		return sResultPath;


	}

	@Keyword
	public String generateRandomNumber()
	{
		Random rnd = new Random()
		def randomNumber = ((10000 + rnd.nextInt(250000)) * 100000)
		String randproviderIdentifier = 'KatlonAPI-' + randomNumber +'-Automation'
	}

	//This function returns a date in string format with offset of x days from current date
	@Keyword
	public String getDeltaDateDays(int intDays) {

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		try
		{

			cal.add(Calendar.DATE, intDays);
		}

		catch(Exception e)
		{
			e.printStackTrace();

		}
		return dateFormat.format(cal.getTime());
	}
	@Keyword
	public String getInvalidDeltaDateDays(int intDays) {

		DateFormat dateFormat = new SimpleDateFormat("MM-dd-yy");
		Calendar cal = Calendar.getInstance();
		try
		{

			cal.add(Calendar.DATE, intDays);
		}

		catch(Exception e)
		{
			e.printStackTrace();

		}
		return dateFormat.format(cal.getTime());
	}


	//This function gets the current date and return date in string format
	@Keyword
	public String getCurrentDate() {

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String sDate;
		try
		{
			sDate = formatter.format(date);
		}

		catch(Exception e)
		{
			e.printStackTrace();

		}

		return sDate;
	}
	public String getHeaderResponse(fullResponseUrlPOST){


		println('fullResponseUrlPOST:'+fullResponseUrlPOST)
		def isGuid=/[0-9a-zA-Z]{8}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{12}/
		def generatedGuid
		if ( fullResponseUrlPOST =~ isGuid ){
			def matcher = (  fullResponseUrlPOST =~ isGuid )
			generatedGuid  = matcher[0]
		}
		println ('generatedGuid:'+generatedGuid )
		return generatedGuid;
	}


	@Keyword
	public  String getSaltNumber(int length) {
		StringBuilder ssalt = new StringBuilder();
		try
		{
			int intleftLimit = 49; // number '1'
			int intrightLimit = 57; // number '9'
			Random rnd = new Random();
			for (int i = 0; i < length; i++)
			{
				ssalt.append((char) (intleftLimit + rnd.nextDouble() * (intrightLimit - intleftLimit)));
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return ssalt.toString();
	}

	//This function gets No of length and return random string of that length
	@Keyword
	public String getSaltString(int intlength) {
		String ssaltStr = ""
		try
		{
			int leftLimit = 65; // letter 'A'
			int rightLimit = 90; // letter 'Z'
			StringBuilder ssalt = new StringBuilder();
			Random rnd = new Random();
			while (ssalt.length() < intlength) {
				int randomLimitedInt = leftLimit + (int) (rnd.nextFloat() * (rightLimit - leftLimit + 1));
				ssalt.append((char) randomLimitedInt);
			}
			ssaltStr = ssalt.toString();
		}

		catch(Exception e)
		{
			e.printStackTrace();

		}
		return ssaltStr;
	}

	@Keyword
	public Boolean CompareLists(ArrayList lst1 , ArrayList lst2)
	{
		Collections.sort(lst1);
		Collections.sort(lst2);


		Boolean bEquals = lst1.equals(lst2);
		return bEquals;
	}
	@Keyword
	public boolean isThisDateValid(String dateToValidate, String dateFromat){

		if(dateToValidate == null){
			return false;
		}

		SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
		sdf.setLenient(false);


		try{
			//if not valid, it will throw ParseException
			Date date = sdf.parse(dateToValidate);
			System.out.println(date);
		}catch(ParseException e){
			e.printStackTrace()
		}
		return true;
	}
}

