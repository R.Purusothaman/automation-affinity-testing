<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_USBank_ThankYou_AgentReachOutMsg</name>
   <tag></tag>
   <elementGuidId>dfffb160-4e76-4662-8243-35aa1430e4a7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot; _modalScreen5Thanks&quot;]//*[@class=&quot;part success&quot;]//p[contains(text(),'A program representative will be reaching out shortly to connect you with a real estate agent.')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
