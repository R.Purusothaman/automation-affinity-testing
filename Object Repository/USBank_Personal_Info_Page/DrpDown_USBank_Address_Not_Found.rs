<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DrpDown_USBank_Address_Not_Found</name>
   <tag></tag>
   <elementGuidId>3c7e62f4-c317-452f-84e2-c2a63841ff13</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//*[@id=&quot;results&quot;]//li[contains(text(),'Address cannot be found')])[1]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
