<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_USBank_ThankYouPageText</name>
   <tag></tag>
   <elementGuidId>c37d5736-94ad-40e9-8b3c-7e9cf7a52e3c</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot; _modalScreen5Thanks&quot;]//*[@class=&quot;part success&quot;]//h2[@class=&quot;_h2&quot;][contains(text(),'Thank you for connecting with us!')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
