<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_USBank_ThankYou_Success_CloseBtn</name>
   <tag></tag>
   <elementGuidId>41aee286-dd78-4c74-a39b-42383f7d0c65</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot; _modalScreen5Thanks&quot;]//div[@class=&quot;part success&quot;]//button[contains(text(),'Close')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
