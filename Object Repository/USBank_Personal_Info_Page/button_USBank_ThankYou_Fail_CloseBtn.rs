<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_USBank_ThankYou_Fail_CloseBtn</name>
   <tag></tag>
   <elementGuidId>d9ccd381-cef7-40ce-83f1-1815bc5c1c12</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot; _modalScreen5Thanks&quot;]//div[@class=&quot;part fail error&quot;]//button[contains(text(),'Close')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
