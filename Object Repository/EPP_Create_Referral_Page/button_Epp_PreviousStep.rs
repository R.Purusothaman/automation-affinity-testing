<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Epp_PreviousStep</name>
   <tag></tag>
   <elementGuidId>313ae215-1a56-490e-be12-049dacac2a4f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//li[@data-index=&quot;4&quot;]//*[@class=&quot;button-area&quot;]//button[contains(text(),'Previous Step')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
