<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Epp_Please_provide_employer_name</name>
   <tag></tag>
   <elementGuidId>5cf7c12d-713d-4e11-833b-ff67515e18d8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@class=&quot; _modalScreenExtra&quot;]//form//input[@placeholder=&quot;Please provide your employer name and/or organization? &quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
