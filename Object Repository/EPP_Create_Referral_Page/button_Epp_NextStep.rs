<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Epp_NextStep</name>
   <tag></tag>
   <elementGuidId>ed7b0829-c066-4a2b-af7c-38bc95c9b88a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//li[@data-index=&quot;4&quot;]//*[@class=&quot;button-area&quot;]//button[contains(text(),'Next Step')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
