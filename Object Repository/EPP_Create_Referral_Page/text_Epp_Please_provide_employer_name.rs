<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Epp_Please_provide_employer_name</name>
   <tag></tag>
   <elementGuidId>be20e855-9558-4d12-b125-10362f9169ee</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@class=&quot; _modalScreenExtra&quot;]//form//p[contains(text(),'Please provide your employer name and/or organization? ')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
