<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>text_Guraranteedrate_ThankYou_AgentReachOutMsg</name>
   <tag></tag>
   <elementGuidId>f4095018-7da7-4b78-80d1-a3c279dec4ca</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot; _modalScreen5Thanks&quot;]//*[@class=&quot;part success&quot;]//p[contains(text(),'A program representative will be reaching out shortly.')]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
