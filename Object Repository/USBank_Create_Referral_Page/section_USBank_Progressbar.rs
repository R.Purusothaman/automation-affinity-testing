<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>section_USBank_Progressbar</name>
   <tag></tag>
   <elementGuidId>278da380-17ca-416a-8b61-ff94a1622ce3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@class=&quot;pg-bar-container desktop&quot;]//div[@role=&quot;progressbar&quot;]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.desktop > div[role=&quot;progressbar&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
