<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test_Suite_RLG_Sites_Sprint_39</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient>R.Purusothaman@Cartus.com;R.Purusothaman@mindtree.com;</mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>a5a8a2d3-7519-446a-800f-a8f098a4514a</testSuiteGuid>
   <testCaseLink>
      <guid>7afce503-8d31-40b8-bdd3-5a23680c4f7c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_USBANK_Both_flow/AF_USBANK_E2E_TC_Both_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>586b5220-696a-44ee-802f-1ee4c9dc0056</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_USBANK_Both_flow/AF_USBANK_E2E_TC_Both_2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bc79f4d7-61e8-442f-884d-085e9f23f038</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_USBANK_Both_flow/AF_USBANK_E2E_TC_Both_3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f4819e09-6d61-4ef8-b14f-ea19ad6c1925</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_USBANK_Both_flow/AF_USBANK_E2E_TC_Both_4</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>64760915-0d79-4053-a446-c12cdd38f6aa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_USBANK_Both_flow/AF_USBANK_E2E_TC_Both_5</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b49b21d2-f39d-48e8-b766-02f576cfa678</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_USBANK_Both_flow/AF_USBANK_E2E_TC_Both_6</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4eb8c84e-2f2e-480f-a701-48cb6fb8c9c9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_USBANK_Both_flow/AF_USBANK_E2E_TC_Both_7</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b0fe59b6-ddce-4d9e-9b75-7918250c5143</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_USBANK_Both_flow/AF_USBANK_E2E_TC_Both_8</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dd2ac4f5-cb1c-4541-b592-76fe20303252</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_USBANK_Both_flow/AF_USBANK_E2E_TC_Both_9</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e2be1794-57db-433f-b8fe-34b40cdaa8f6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_USBANK_Buy_flow/AF_USBANK_E2E_TC_Buy_1</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>b20921ea-5690-4d86-b681-29fd536451c8</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>d56df05f-8a0b-4336-972b-c920514e7c3d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_USBANK_Buy_flow/AF_USBANK_E2E_TC_Buy_2</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c7cad1f5-b551-4bc5-99e2-42bd31cd5b1a</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>2b692adb-e746-4e8a-a0e4-67f64c4fab65</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_USBANK_Buy_flow/AF_USBANK_E2E_TC_Buy_3</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>efc3c476-08bc-45cc-9e38-16689a8fd19c</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>d1bed896-6810-4d92-8884-410354fdaaca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_USBANK_Buy_flow/AF_USBANK_E2E_TC_Buy_4</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>17ad15a5-62dd-42b6-8bcc-f9fbdc8fe65d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>014887cb-1065-4e83-8702-623e95759e65</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>b2dc787f-5820-4a51-b379-f4d1b63e9665</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_USBANK_Buy_flow/AF_USBANK_E2E_TC_Buy_5</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>87ce9cf9-10dd-4e9f-a579-5ab87adfe47b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>45d08bd3-98c4-46f9-98dc-e567cea383e6</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>3fe15f24-4b34-4c83-8cd5-d3b930b5616f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_USBANK_Buy_flow/AF_USBANK_E2E_TC_Buy_6</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d72b5231-ef9f-4007-8af2-d62fd2642303</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>2dab1d0b-cc17-414d-af85-8dc5169b114a</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>42254f79-cabb-4fb7-8220-f95dbc71b1a9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_USBANK_Buy_flow/AF_USBANK_E2E_TC_Buy_7</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7a861ae0-7b9f-49e5-99cd-d8df889d45e7</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>3675384e-b057-48d0-8eb5-1ab3da27b2c3</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>195e77f1-b25e-48dc-b29e-14cf0221c4ba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_USBANK_Buy_flow/AF_USBANK_E2E_TC_Buy_8</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c1049a22-3ef7-44c9-b99a-41f5ee529ccf</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>a0b18573-91a7-4ace-99f8-d65a9efd6a72</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_USBANK_Buy_flow/AF_USBANK_E2E_TC_Buy_9</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d723412f-90d4-4b63-a272-035a3050d14f</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>cec69189-80c3-457d-bf35-9f186ad349c8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_USBANK_Sell_flow/AF_USBANK_E2E_TC_Sell_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c8d16454-29ef-4bf9-a6d6-46cb00fcd8b0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_USBANK_Sell_flow/AF_USBANK_E2E_TC_Sell_2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cf9fe3ce-4394-44bd-96a2-2c79c51a83a1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_USBANK_Sell_flow/AF_USBANK_E2E_TC_Sell_3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5edf3501-a929-4841-9b4e-576416fbfd39</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_USBANK_Sell_flow/AF_USBANK_E2E_TC_Sell_4</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2d9b3df0-1d10-4325-b7e9-f7b0225bb099</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_USBANK_Sell_flow/AF_USBANK_E2E_TC_Sell_5</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6c827f32-5c6e-4a1b-a9e2-e15a69bbae3e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_USBANK_Sell_flow/AF_USBANK_E2E_TC_Sell_6</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e26ad867-3d21-4b3a-bab3-1056a11d1368</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_USBANK_Sell_flow/AF_USBANK_E2E_TC_Sell_7</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>022e28ea-1704-4fd6-b35a-919ef1b5414f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_USBANK_Sell_flow/AF_USBANK_E2E_TC_Sell_8</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9eb542d6-2129-4bd8-a6ce-6d472434ab1e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_USBANK_Sell_flow/AF_USBANK_E2E_TC_Sell_9</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d2eed8c1-4cba-4b8f-bee8-3e6586fce80f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_AARP_Both_flow/AF_AARP_E2E_TC_Both_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2940ebf4-6341-4faa-af88-f00b40794a0d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_AARP_Both_flow/AF_AARP_E2E_TC_Both_2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2c22c46d-ba9f-40c2-81d7-18d4df235824</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_AARP_Both_flow/AF_AARP_E2E_TC_Both_3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a3597c25-6276-4339-8f46-ab125cd27234</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_AARP_Both_flow/AF_AARP_E2E_TC_Both_4</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bfdc9c8c-aede-45b4-a098-11a624ee6f07</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_AARP_Both_flow/AF_AARP_E2E_TC_Both_5</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2dad62de-c79a-4014-a0cd-fddc526c03c3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_AARP_Both_flow/AF_AARP_E2E_TC_Both_6</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a9e55167-911e-4ef6-8cc9-e93bf73c722c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_AARP_Both_flow/AF_AARP_E2E_TC_Both_7</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eaa9bb01-55d8-46e8-aa93-bd29e9b18329</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_AARP_Both_flow/AF_AARP_E2E_TC_Both_8</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a224b6e3-afab-4bb4-90fb-c86cb4820a7e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_AARP_Both_flow/AF_AARP_E2E_TC_Both_9</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3dbcfca3-0def-4f3e-bbd5-b952176e3b53</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_AARP_Buy_flow/AF_AARP_E2E_TC_Buy_1</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>55d817f0-335b-41e9-8491-5034a42e60d2</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>a51fb0f9-ceb1-4a69-829f-1feb49a0d16f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_AARP_Buy_flow/AF_AARP_E2E_TC_Buy_2</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d3c015fd-b898-4f4d-8693-5f9bd709561f</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>07bd0e6f-9143-4888-89d8-67eeeb70ecb0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_AARP_Buy_flow/AF_AARP_E2E_TC_Buy_3</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ee0f2794-59ce-4717-a9b5-55f3e34917d9</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>a8f5df67-35af-4928-9e7c-462e3d306afe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_AARP_Buy_flow/AF_AARP_E2E_TC_Buy_4</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>aa07f537-9d67-465b-a1c6-e881a1e54d38</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>57b175b4-1927-4aaa-bfc5-05a965ecb0b9</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>9786e5ab-4e34-4a15-9556-4cac03455e00</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_AARP_Buy_flow/AF_AARP_E2E_TC_Buy_5</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>69d1e18f-94b4-4a67-af0a-140d7fdfaf60</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>b41adb77-5367-460f-a80a-0e619882537f</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>529db1ca-0814-413b-b58a-e6d2de23c814</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_AARP_Buy_flow/AF_AARP_E2E_TC_Buy_6</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>be22819c-4594-4607-b853-3782062eaab1</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>e35e86c4-5e02-48a7-8088-454f3223b54d</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>94a29599-4381-479d-a18a-21707e7250f2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_AARP_Buy_flow/AF_AARP_E2E_TC_Buy_7</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>b77d931d-1269-4650-8538-2b848c89f6e6</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d6c9eec6-164b-402e-99e5-cf14ecc51738</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>e572879c-b411-4746-bb67-ee24d6b02ed2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_AARP_Buy_flow/AF_AARP_E2E_TC_Buy_8</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>0f3c963d-9ecd-4c46-9706-e995ff9a0236</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>2900093a-a0fa-44fa-9bd9-450fc449161d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_AARP_Buy_flow/AF_AARP_E2E_TC_Buy_9</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>0f5a9699-3112-427b-8440-23835ffd13d5</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>5230a70e-47f3-4827-a77b-c8e521764a7b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_AARP_Sell_flow/AF_AARP_E2E_TC_Sell_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>58a144df-2575-4f98-bd42-edfdbff57bd5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_AARP_Sell_flow/AF_AARP_E2E_TC_Sell_2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1f303ce3-2162-4051-9377-d22b7aba317b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_AARP_Sell_flow/AF_AARP_E2E_TC_Sell_3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6bca1999-06e6-4dfc-b88c-3fcdf16113be</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_AARP_Sell_flow/AF_AARP_E2E_TC_Sell_4</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3767876a-dbb8-4065-b77c-7ac91afee29c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_AARP_Sell_flow/AF_AARP_E2E_TC_Sell_5</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ca5cc78b-8334-4f46-b820-47288777d25b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_AARP_Sell_flow/AF_AARP_E2E_TC_Sell_6</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8b688f2d-df50-41e8-a70f-e6d4e4a0ca75</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_AARP_Sell_flow/AF_AARP_E2E_TC_Sell_7</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2d0d3388-2967-4cb6-86ef-b8b3d40c879a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_AARP_Sell_flow/AF_AARP_E2E_TC_Sell_8</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b8953a8b-feb2-45b2-ab78-757cd3599dc7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_AARP_Sell_flow/AF_AARP_E2E_TC_Sell_9</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>09375775-6211-4966-8e0e-34d91b548c0d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Associa_Both_flow/AF_Associa_E2E_TC_Both_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a7045d39-2804-4b43-8562-78485ca2bc00</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Associa_Both_flow/AF_Associa_E2E_TC_Both_2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>794e182d-64b8-40aa-b10a-a41f46690f13</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Associa_Both_flow/AF_Associa_E2E_TC_Both_3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8ce9fd98-2349-49c9-a321-445604e01ef6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Associa_Both_flow/AF_Associa_E2E_TC_Both_4</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bdaabfb0-deb0-48e9-b8cc-2f74d045de49</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Associa_Both_flow/AF_Associa_E2E_TC_Both_5</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>46bbccc4-ce4d-4c7d-ad2a-879c6e7f8eb4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Associa_Both_flow/AF_Associa_E2E_TC_Both_6</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0098be04-c1fd-4a33-9505-ef432fe85241</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Associa_Both_flow/AF_Associa_E2E_TC_Both_7</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b9673362-cc27-4c7c-8675-ea3a7da8c5a5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Associa_Both_flow/AF_Associa_E2E_TC_Both_8</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e7d2014b-2cf3-4177-b4ed-97c539588bce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Associa_Both_flow/AF_Associa_E2E_TC_Both_9</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c671c262-88cb-4638-ad98-bd08122f0e8e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Associa_Buy_flow/AF_Associa_E2E_TC_Buy_1</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ad2fd208-e010-4f58-b7d1-0d84aef83bc9</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>cb5e76d7-b44b-4452-a47b-0a402d53bf9a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Associa_Buy_flow/AF_Associa_E2E_TC_Buy_2</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>df35aca2-a494-4892-a215-31f368244c67</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>6419d091-9a2e-4db7-bc54-14c2659a0777</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Associa_Buy_flow/AF_Associa_E2E_TC_Buy_3</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>45bfcaa7-5532-43a7-8bf4-640f05dcf8b1</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>fd11bede-5b39-4395-9309-add7db0f6f10</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Associa_Buy_flow/AF_Associa_E2E_TC_Buy_4</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>15faed84-9019-402f-b46f-75003610088c</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ec288224-20aa-4fc1-a636-968fb528848a</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>1b1fd8d4-12ac-49e2-8cf3-38d758a0cc3a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Associa_Buy_flow/AF_Associa_E2E_TC_Buy_5</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>3a768d46-1a65-4ec9-b6fe-35de3c58b382</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c04ff2fe-b44a-4669-990a-8f5c00d76ca5</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>2a32fa76-cc4a-4749-9767-c1433d956d64</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Associa_Buy_flow/AF_Associa_E2E_TC_Buy_6</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>8d989e56-155e-415d-bfc9-25042db934ae</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>24e40e08-48db-4038-a76f-d68f21969df7</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>b997a4f2-df4e-4b79-9b6d-2b2310116860</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Associa_Buy_flow/AF_Associa_E2E_TC_Buy_7</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>52cbcddc-0192-465e-8191-b9a29a5acd74</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d210116a-8e3e-482f-a487-c7e3edd8f8ee</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>97a41c4c-8906-4991-8bbb-a27e03758805</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Associa_Buy_flow/AF_Associa_E2E_TC_Buy_8</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>9f24c46b-a031-4e53-9155-ff6c2bff0639</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>9630547d-cbf4-4e98-bd5b-316af11001b7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Associa_Buy_flow/AF_Associa_E2E_TC_Buy_9</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d34597db-2a46-4c81-b21c-45bc7b744efc</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>db869c56-cd39-456b-bb9a-2928ef144396</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Associa_Sell_flow/AF_Associa_E2E_TC_Sell_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9be4590e-6e02-465a-94d9-1d8ba7e62e93</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Associa_Sell_flow/AF_Associa_E2E_TC_Sell_2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5d7913a1-a906-468a-ac5c-b04986e12c4b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Associa_Sell_flow/AF_Associa_E2E_TC_Sell_3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aa0501ca-481d-4abe-a125-c8d449601369</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Associa_Sell_flow/AF_Associa_E2E_TC_Sell_4</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>91b71f6d-3218-47da-9c7b-f766af15afb2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Associa_Sell_flow/AF_Associa_E2E_TC_Sell_5</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e89bb201-dfef-4551-884e-5a33c2ffeb3a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Associa_Sell_flow/AF_Associa_E2E_TC_Sell_6</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3838cbeb-68f3-4a85-a8f6-8affa792bbc4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Associa_Sell_flow/AF_Associa_E2E_TC_Sell_7</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3ca73f53-33dc-433e-a0c9-14613d5abac8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Associa_Sell_flow/AF_Associa_E2E_TC_Sell_8</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a99cdbe4-7ac6-4c69-adfb-3ae6049aad56</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Associa_Sell_flow/AF_Associa_E2E_TC_Sell_9</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9776d61d-c126-49b4-a35b-5a97bec966e2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_American_Airlines_Both_flow/AF_American_Airlines_E2E_TC_Both_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>80f9a0d0-d889-4936-802e-9878777dd1a8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_American_Airlines_Both_flow/AF_American_Airlines_E2E_TC_Both_2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c7f248d5-77e4-4ac7-8c0c-ddacfc86271e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_American_Airlines_Both_flow/AF_American_Airlines_E2E_TC_Both_3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>23fdfcc0-c9b8-4004-ae45-fa5984f274ac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_American_Airlines_Both_flow/AF_American_Airlines_E2E_TC_Both_4</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f10da583-bc01-402f-9aae-7e7081dcc537</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_American_Airlines_Both_flow/AF_American_Airlines_E2E_TC_Both_5</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>346a254d-2405-419f-8097-a5e142ba2402</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_American_Airlines_Both_flow/AF_American_Airlines_E2E_TC_Both_6</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f84bf423-8e1b-4626-95d5-0215599e35c4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_American_Airlines_Both_flow/AF_American_Airlines_E2E_TC_Both_7</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6237d449-60ac-4fa4-b9d4-10b1f37b1953</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_American_Airlines_Both_flow/AF_American_Airlines_E2E_TC_Both_8</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0ecfa35c-0ab4-45b2-b1e5-c22c3882e451</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_American_Airlines_Both_flow/AF_American_Airlines_E2E_TC_Both_9</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>93be91e7-36d7-4765-8e24-4ee724a223e5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_American_Airlines_Buy_flow/AF_American_Airlines_E2E_TC_Buy_1</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d3a71fa7-1a50-4111-b0ac-6636c28f2a67</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>3c9e2ee9-a4d2-49aa-be8b-55e86f1410a2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_American_Airlines_Buy_flow/AF_American_Airlines_E2E_TC_Buy_2</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c8aed534-193a-43ca-b5b1-8baaa2c6ef3e</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>a6f12beb-efc5-4564-a606-7a9a2dbbdd6e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_American_Airlines_Buy_flow/AF_American_Airlines_E2E_TC_Buy_3</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>39e78807-21ba-4df4-8258-7abd2442b7e0</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>baa0246c-aa13-4fb1-8bc8-e4160344e823</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_American_Airlines_Buy_flow/AF_American_Airlines_E2E_TC_Buy_4</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>4acb1488-8cd0-4e6c-8c65-b3917a9ca0e0</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ee32705e-6907-4a45-87e3-e1ee1ddafbef</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>38fade9d-6ce9-44a0-9db7-cdc263a69135</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_American_Airlines_Buy_flow/AF_American_Airlines_E2E_TC_Buy_5</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f081d6bd-c341-42c0-94a4-993841cfa00a</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>bff7ebee-b869-4b95-8e80-34fb55eccc5c</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>a635e1f3-dcd3-4bea-a287-6340907a22ba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_American_Airlines_Buy_flow/AF_American_Airlines_E2E_TC_Buy_6</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c1a394a5-c46d-4d42-b3bf-00b5ce1232b8</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>34485ed2-f775-40a2-9ba9-441fbae28ec2</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>536b7d42-883d-4a35-bb4a-4869e533614d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_American_Airlines_Buy_flow/AF_American_Airlines_E2E_TC_Buy_7</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ba9a27c5-983b-49e3-830a-f3c931f4df5e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>6840f1c4-27b9-4c9a-ab2f-7a56f66ef8eb</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>88981a1e-c52b-4133-8645-4e1de2069ee2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_American_Airlines_Buy_flow/AF_American_Airlines_E2E_TC_Buy_8</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>96753248-63cf-418e-8cb9-3f67fdb43c5c</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>63eb90b3-2419-4581-976f-093a281b816c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_American_Airlines_Buy_flow/AF_American_Airlines_E2E_TC_Buy_9</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>4fefa08f-1594-47a6-ba5c-9787beea6b87</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>70ec906f-5c64-4b2f-b019-828112bb9a9c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_American_Airlines_Sell_flow/AF_American_Airlines_E2E_TC_Sell_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c1f61a91-f6b2-4f4f-844e-22ae87135e24</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_American_Airlines_Sell_flow/AF_American_Airlines_E2E_TC_Sell_2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7623f666-3409-44ba-b09e-13510ab805b2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_American_Airlines_Sell_flow/AF_American_Airlines_E2E_TC_Sell_3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a1fd5d46-5893-49df-9e11-0d5d87a7e674</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_American_Airlines_Sell_flow/AF_American_Airlines_E2E_TC_Sell_4</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5d12e2d8-ca7e-4faf-b0b6-70d91cb469fa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_American_Airlines_Sell_flow/AF_American_Airlines_E2E_TC_Sell_5</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1030a898-526d-48d8-82b9-02e4b42e0bdd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_American_Airlines_Sell_flow/AF_American_Airlines_E2E_TC_Sell_6</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c6d56dde-c9db-470c-9b32-422183fce3e8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_American_Airlines_Sell_flow/AF_American_Airlines_E2E_TC_Sell_7</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>687da104-5061-4439-9a31-98064e7a457c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_American_Airlines_Sell_flow/AF_American_Airlines_E2E_TC_Sell_8</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>12a4f3bf-1731-4022-8c1e-a992af25cf7b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_American_Airlines_Sell_flow/AF_American_Airlines_E2E_TC_Sell_9</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d7b17566-ac10-4fb5-a566-fd98d8305632</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Realogy_Leads_Groups_Both_flow/AF_RLG_E2E_TC_Both_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1f4f1a6f-e745-4526-97ee-2efa88b51ec1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Realogy_Leads_Groups_Both_flow/AF_RLG_E2E_TC_Both_2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c8f02147-b000-4edd-8932-7a3d395f6bc7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Realogy_Leads_Groups_Both_flow/AF_RLG_E2E_TC_Both_3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7b25d47a-9409-405e-88e0-f16f576e83b7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Realogy_Leads_Groups_Both_flow/AF_RLG_E2E_TC_Both_4</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a96e5057-7ad2-4c4c-8d0d-a1e6623ccff1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Realogy_Leads_Groups_Both_flow/AF_RLG_E2E_TC_Both_5</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>594318e7-b744-4cad-b623-4ed3f90a9a5e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Realogy_Leads_Groups_Both_flow/AF_RLG_E2E_TC_Both_6</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fa7bc89b-aca6-49d6-9563-8ab01e37b9e0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Realogy_Leads_Groups_Both_flow/AF_RLG_E2E_TC_Both_7</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f5a82891-60ec-4226-af16-10aad64d7de2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Realogy_Leads_Groups_Both_flow/AF_RLG_E2E_TC_Both_8</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>337da961-318e-4df8-b365-4fa5f8da54c9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Realogy_Leads_Groups_Both_flow/AF_RLG_E2E_TC_Both_9</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>699f42fc-4e60-446e-83c9-7fb35894ae30</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Realogy_Leads_Groups_Buy_flow/AF_RLG_E2E_TC_Buy_1</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>02a1b14b-2117-4d34-9b2c-ef4b0f6dc214</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>0cc44ac3-f592-4202-a440-252581414aa9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Realogy_Leads_Groups_Buy_flow/AF_RLG_E2E_TC_Buy_2</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>b95649e2-927e-43a6-b0d0-7600fc8faf5e</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>85236cb0-fd56-44d9-a106-1d41dc5e5495</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Realogy_Leads_Groups_Buy_flow/AF_RLG_E2E_TC_Buy_3</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>26c1471d-e41d-4d49-82a1-a68e39617e32</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>8b4d7853-7d8d-411c-8aab-c14088a10fb6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Realogy_Leads_Groups_Buy_flow/AF_RLG_E2E_TC_Buy_4</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>092e8d2d-1301-43a8-bb03-b71311dcaf5b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>1a260ed4-b416-40de-b722-3ed0b783f3a9</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>63050aff-38db-4947-b391-83c70f00aada</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Realogy_Leads_Groups_Buy_flow/AF_RLG_E2E_TC_Buy_5</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>4e2d55e3-c737-4542-9b7c-0e30b5cb652b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7db6e602-8b80-4eae-9b71-3ec1075719dd</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>00edebc0-c831-45a8-9d08-3c6f0381bc0c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Realogy_Leads_Groups_Buy_flow/AF_RLG_E2E_TC_Buy_6</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>53e723d8-e658-4158-865c-a99a9200737b</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c57d8588-535e-4177-9ad4-54bcb1b0cc3c</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>41af1986-7139-4da2-9236-6a18895bf2e7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Realogy_Leads_Groups_Buy_flow/AF_RLG_E2E_TC_Buy_7</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d43b46b6-f55e-433b-8f31-68e8beb145f6</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>5100adb2-8e7c-4c16-97df-287e97e65f6e</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>cc405c3c-bdc5-407d-ad76-d8ad90faac53</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Realogy_Leads_Groups_Buy_flow/AF_RLG_E2E_TC_Buy_8</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>171d3807-dfc0-49df-bac6-aab321ee090d</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>52e6b771-4e6a-4df5-8199-14d06f5a9eb8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Realogy_Leads_Groups_Buy_flow/AF_RLG_E2E_TC_Buy_9</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>e2fc8168-e1da-493b-a2db-944ccf4f41be</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>9c24e487-c67d-4b31-ab05-d18a7f7820a6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Realogy_Leads_Groups_Sell_flow/AF_RLG_E2E_TC_Sell_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c33d4b22-1ab3-484f-a53c-6941c329b3c0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Realogy_Leads_Groups_Sell_flow/AF_RLG_E2E_TC_Sell_2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8f932aab-0a89-4f95-9b16-fcdfe11182ca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Realogy_Leads_Groups_Sell_flow/AF_RLG_E2E_TC_Sell_3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c15d86cc-bdfb-4588-ab13-984d584741fa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Realogy_Leads_Groups_Sell_flow/AF_RLG_E2E_TC_Sell_4</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e5ed05c1-ab29-4434-98d3-c10f744e0ef0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Realogy_Leads_Groups_Sell_flow/AF_RLG_E2E_TC_Sell_5</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>44a1ffe9-b740-4ab2-a17e-0f27a8863fbd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Realogy_Leads_Groups_Sell_flow/AF_RLG_E2E_TC_Sell_6</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9c775e5a-c03b-40f8-a0ca-7dea104dcf17</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Realogy_Leads_Groups_Sell_flow/AF_RLG_E2E_TC_Sell_7</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d02e14ab-b2c3-4add-8573-24f3821cc5d1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Realogy_Leads_Groups_Sell_flow/AF_RLG_E2E_TC_Sell_8</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>533147e9-9023-4cf3-b7c3-f7aa1d1e517d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Realogy_Leads_Groups_Sell_flow/AF_RLG_E2E_TC_Sell_9</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9c8cbaf4-e425-4973-abc3-d0d35d51bb58</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Guaranteedrate_Both_flow/AF_Guaranteedrate_E2E_TC_Both_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9715d4c3-381d-45cc-9b2a-777f14f5f14b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Guaranteedrate_Both_flow/AF_Guaranteedrate_E2E_TC_Both_2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c971db2d-faa5-489e-b9d1-9cf11ff4b1e9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Guaranteedrate_Both_flow/AF_Guaranteedrate_E2E_TC_Both_3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>52e174cb-fb3e-4616-9ab2-e239b5db4844</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Guaranteedrate_Both_flow/AF_Guaranteedrate_E2E_TC_Both_4</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>136979c0-0eee-4fc6-8370-0f4835411ba0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Guaranteedrate_Both_flow/AF_Guaranteedrate_E2E_TC_Both_5</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>73a6d8ae-bc3e-43c7-91ea-680329b2ebd8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Guaranteedrate_Both_flow/AF_Guaranteedrate_E2E_TC_Both_6</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>88890063-ad0c-4e6e-afdd-16dc42963609</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Guaranteedrate_Both_flow/AF_Guaranteedrate_E2E_TC_Both_7</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fbc22651-9582-4e29-b527-fbc88676ac6b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Guaranteedrate_Both_flow/AF_Guaranteedrate_E2E_TC_Both_8</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b1aaf1d3-0b4e-4c10-b9af-c4ee28b618fc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Guaranteedrate_Both_flow/AF_Guaranteedrate_E2E_TC_Both_9</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ea6ac1b3-8c5f-40c4-a441-7dfa92bf8b07</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Guaranteedrate_Buy_flow/AF_Guaranteedrate_E2E_TC_Buy_1</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>52a92b35-622d-4200-939e-78f9371ab46c</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>59b94108-bdd9-48a0-91e9-d74b429a29f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Guaranteedrate_Buy_flow/AF_Guaranteedrate_E2E_TC_Buy_2</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>cf7918e0-b471-448f-b5e2-e19d679798af</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>1547c867-b97d-4c50-99c7-b9ce2638024c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Guaranteedrate_Buy_flow/AF_Guaranteedrate_E2E_TC_Buy_3</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>4dc6e870-19df-4288-8c3e-7b71619020ad</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>c8283af5-2ee9-4070-beab-3b2ffbe02430</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Guaranteedrate_Buy_flow/AF_Guaranteedrate_E2E_TC_Buy_4</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>03020902-3142-4b14-9e2d-7a5cd035f245</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a5b2facf-d0c3-4f3d-9340-d472bcf4d412</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>aaa1aedf-038b-4717-a357-ae9f020cb559</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Guaranteedrate_Buy_flow/AF_Guaranteedrate_E2E_TC_Buy_5</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>950f0cd9-f65a-4dc5-bfd4-c35a2d5ba000</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ed1f55c1-4f23-49c2-b6e3-5b19bf5b946d</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>7b48fd19-9b8a-48a4-b469-c7efbe639ef4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Guaranteedrate_Buy_flow/AF_Guaranteedrate_E2E_TC_Buy_6</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>27d667cf-dabe-421b-8942-b0613577a131</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ceb55b52-3d37-414a-bec0-881a3c478c85</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>fd08d3ab-845e-412d-a505-82db6ebfe264</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Guaranteedrate_Buy_flow/AF_Guaranteedrate_E2E_TC_Buy_7</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d91d2bd4-f3af-4942-901f-a1455641c68e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>fe88a0ab-f15e-40b0-9ccf-f91064aa8187</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>504a44ae-bada-4dac-bdb1-580a4d3caad7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Guaranteedrate_Buy_flow/AF_Guaranteedrate_E2E_TC_Buy_8</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>142cb862-5cf6-43d7-aa09-9a9e23c84afb</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>2d7565bb-adcc-467f-af58-2d8875d68d82</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Guaranteedrate_Buy_flow/AF_Guaranteedrate_E2E_TC_Buy_9</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>b36d711b-f24b-45be-a9ea-8572c9fff9a6</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>b1a424c7-9997-48c2-83b4-ec4a4796749b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Guaranteedrate_Sell_flow/AF_Guaranteedrate_E2E_TC_Sell_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c7bdcb82-2294-4723-9671-9e70505b0cb8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Guaranteedrate_Sell_flow/AF_Guaranteedrate_E2E_TC_Sell_2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eaead6fc-0862-4223-9d57-31246dc04234</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Guaranteedrate_Sell_flow/AF_Guaranteedrate_E2E_TC_Sell_3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>05f2dc9e-40b6-407a-90da-f442e783dd69</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Guaranteedrate_Sell_flow/AF_Guaranteedrate_E2E_TC_Sell_4</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eb298a81-91c2-4c1b-bb52-4539c759179e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Guaranteedrate_Sell_flow/AF_Guaranteedrate_E2E_TC_Sell_5</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3908ef8e-4735-4c09-b4a7-c47617c7a118</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Guaranteedrate_Sell_flow/AF_Guaranteedrate_E2E_TC_Sell_6</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>29b7cd96-4064-4598-bf80-4e1e0ad630ed</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Guaranteedrate_Sell_flow/AF_Guaranteedrate_E2E_TC_Sell_7</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6dc8e8d5-93e0-4491-8b97-21156ecd0d52</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Guaranteedrate_Sell_flow/AF_Guaranteedrate_E2E_TC_Sell_8</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7439a4bf-afee-418d-80bf-cac0c740aa90</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Guaranteedrate_Sell_flow/AF_Guaranteedrate_E2E_TC_Sell_9</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eaa5c09f-7c97-4a6f-9625-9c828da9f0bb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_BenefitHub_Both_flow/AF_BenefitHub_E2E_TC_Both_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>18469feb-ac53-4e9e-b4b7-5601a8c1ee01</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_BenefitHub_Both_flow/AF_BenefitHub_E2E_TC_Both_2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>58918d11-1a03-4ae5-b7e3-bdb61ea503b8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_BenefitHub_Both_flow/AF_BenefitHub_E2E_TC_Both_3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3bdc9baf-73ac-473d-8792-d41e38728a98</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_BenefitHub_Both_flow/AF_BenefitHub_E2E_TC_Both_4</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9686f92b-ab68-4202-bebe-123561fab52d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_BenefitHub_Both_flow/AF_BenefitHub_E2E_TC_Both_5</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>50db75bb-a33f-4669-930a-8d6c4cddc261</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_BenefitHub_Both_flow/AF_BenefitHub_E2E_TC_Both_6</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>00e2aace-2d7e-40e0-bf3a-38232f0e28ea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_BenefitHub_Both_flow/AF_BenefitHub_E2E_TC_Both_7</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9831fe28-a085-4f3c-b08e-44691615e73a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_BenefitHub_Both_flow/AF_BenefitHub_E2E_TC_Both_8</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b91e26dc-a73c-4f9d-8927-f7813278adc8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_BenefitHub_Both_flow/AF_BenefitHub_E2E_TC_Both_9</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6db5e7f0-f96c-4abf-9263-5214abc13e84</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_BenefitHub_Buy_flow/AF_BenefitHub_E2E_TC_Buy_1</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ffbae0e8-a86d-43bc-b4f1-a104b1369c70</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>83e1b4ba-fe79-4f81-8ee0-17d557d8586b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_BenefitHub_Buy_flow/AF_BenefitHub_E2E_TC_Buy_2</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>5c5ecb26-fe0e-41c2-8170-f45d21300ad5</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>b182e3f0-ee18-41b1-80d7-224623010141</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_BenefitHub_Buy_flow/AF_BenefitHub_E2E_TC_Buy_3</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c5e34f07-0a46-4b78-ad6e-081f977ce27c</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>957592ae-58cd-46d1-a15a-e74c40cc070d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_BenefitHub_Buy_flow/AF_BenefitHub_E2E_TC_Buy_4</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>9eca41e1-d8bc-49f1-8ab5-a44bb40b4733</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>35939283-ad11-4e65-afe5-1212757b9cbb</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>d3969af0-a013-492d-96ff-abe6ca8df158</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_BenefitHub_Buy_flow/AF_BenefitHub_E2E_TC_Buy_5</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>377f274b-c675-45be-b339-0949a6280de3</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>349ecbf5-9657-4ab0-954b-71bbcb7be170</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>0d58938d-7c47-4823-ba3b-7362e2fd323e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_BenefitHub_Buy_flow/AF_BenefitHub_E2E_TC_Buy_6</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c02af986-52c7-4316-b0bd-c7c4a1cfab3e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>75444c78-931b-4267-9c1f-97fad1609b19</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>76dc1f43-c098-4d4e-93f9-7875aede76d8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_BenefitHub_Buy_flow/AF_BenefitHub_E2E_TC_Buy_7</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>694af998-fe2c-4a15-bdbc-fa54143a6c81</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a6064fa4-53c9-4fa0-8b11-c527d531885a</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>e237780d-a960-4f00-a908-e4f7f40029e1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_BenefitHub_Buy_flow/AF_BenefitHub_E2E_TC_Buy_8</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>1bc1d62c-670b-488e-bfec-0e14d606216a</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>c086f857-7495-4664-9337-12561206b922</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_BenefitHub_Buy_flow/AF_BenefitHub_E2E_TC_Buy_9</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>fcb94ada-a15b-4739-8a0d-549925d0af7d</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>5b56c8c7-502b-43ab-a268-b5091e9bb27d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_BenefitHub_Sell_flow/AF_BenefitHub_E2E_TC_Sell_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>02995ce9-87f0-4de4-b13b-0b7053b39ee5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_BenefitHub_Sell_flow/AF_BenefitHub_E2E_TC_Sell_2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b091f07b-35ff-430f-9d0d-e25bbd3d425e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_BenefitHub_Sell_flow/AF_BenefitHub_E2E_TC_Sell_3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>63a35570-a1ad-473e-96f0-29fc5c57d5c8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_BenefitHub_Sell_flow/AF_BenefitHub_E2E_TC_Sell_4</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8b1b609c-7010-4635-b494-acf2b65b8404</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_BenefitHub_Sell_flow/AF_BenefitHub_E2E_TC_Sell_5</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>56d19384-3155-4af2-adbc-1ffa9ecc0ab5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_BenefitHub_Sell_flow/AF_BenefitHub_E2E_TC_Sell_6</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0449636a-f1db-469b-9067-beb73394e970</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_BenefitHub_Sell_flow/AF_BenefitHub_E2E_TC_Sell_7</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>37f1fda1-415f-42dd-a72e-2d42ccea6b23</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_BenefitHub_Sell_flow/AF_BenefitHub_E2E_TC_Sell_8</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a310e4f6-2d39-4b5b-bc7b-60fbac7afc52</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_BenefitHub_Sell_flow/AF_BenefitHub_E2E_TC_Sell_9</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c1174003-3c1f-42b4-ab1c-66099c75318e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Abenity_Both_flow/AF_Abenity_E2E_TC_Both_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0a3de3ba-0efd-46d3-a3c6-578f949e934b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Abenity_Both_flow/AF_Abenity_E2E_TC_Both_2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>52f186bd-65a4-4565-9fb0-4466afa68681</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Abenity_Both_flow/AF_Abenity_E2E_TC_Both_3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a4c6ab9a-03a2-43f2-9a5d-701b9bf87508</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Abenity_Both_flow/AF_Abenity_E2E_TC_Both_4</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>04f73ed3-c1d1-4771-8de0-137c0f5ebf58</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Abenity_Both_flow/AF_Abenity_E2E_TC_Both_5</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>78a5c4da-2a50-42c5-95b7-d14108f19db4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Abenity_Both_flow/AF_Abenity_E2E_TC_Both_6</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bb90740b-0f61-4ee5-8454-a0062c53e8d0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Abenity_Both_flow/AF_Abenity_E2E_TC_Both_7</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ddb64886-6333-4a9f-93ee-8a070bfbd520</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Abenity_Both_flow/AF_Abenity_E2E_TC_Both_8</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a5bc0077-ce84-4aa8-89e6-ba5391faa957</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Abenity_Both_flow/AF_Abenity_E2E_TC_Both_9</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a07f47f6-8c2c-4ea1-9909-d8b7d89b1ee5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Abenity_Buy_flow/AF_Abenity_E2E_TC_Buy_1</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>199dfe4d-bd6e-4d61-bd91-1fe611b20485</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>893a6d03-711f-479c-8bdb-59454a6143e7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Abenity_Buy_flow/AF_Abenity_E2E_TC_Buy_2</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>16eec6e7-9aa6-484e-a018-45352ac5ce95</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>ae4ffc08-b781-4820-a404-cab3a316bbd4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Abenity_Buy_flow/AF_Abenity_E2E_TC_Buy_3</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>504569aa-f0b0-4b8a-ad8e-afe870463bcd</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>6194f965-3202-43ea-97ea-2a9f8802a733</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Abenity_Buy_flow/AF_Abenity_E2E_TC_Buy_4</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>2e020bce-5f90-464e-af1f-f9851d256371</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ca70c5ef-fa30-4859-ab67-54d320d5d3f3</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>9c685ff0-63f2-43df-8659-ec26f25f6525</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Abenity_Buy_flow/AF_Abenity_E2E_TC_Buy_5</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>2639f004-a518-44a2-a914-58b14a96e0c9</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>09e38b90-0215-47ca-95cc-d629aeaee673</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>a5ac77d6-f561-4803-8900-ff2733dc4a67</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Abenity_Buy_flow/AF_Abenity_E2E_TC_Buy_6</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a3907522-ddb5-4c76-9609-0d12bc9fe573</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>3e26c497-2ab3-4f24-bd13-5d691b98bf49</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>48101af6-7da4-4393-a8a0-705fca1080bc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Abenity_Buy_flow/AF_Abenity_E2E_TC_Buy_7</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>354047af-c90a-4599-a6d0-79a8ded1a8a1</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>99bd14ca-20c5-4893-8ffe-349a92ac8a00</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>a7707a27-51d3-4cb3-b6b8-f46cf9e15f10</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Abenity_Buy_flow/AF_Abenity_E2E_TC_Buy_8</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>33736b60-d123-496c-8f4b-9ba61d088123</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>e31dcda4-3271-4a78-8f3b-ad48a7acd9ce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Abenity_Buy_flow/AF_Abenity_E2E_TC_Buy_9</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>1206354a-d941-46e1-a300-d917cd66c50e</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>8defc247-bbdc-4333-baaa-506231dd88d3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Abenity_Sell_flow/AF_Abenity_E2E_TC_Sell_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5f0f9c07-43fd-48af-a613-869c5b0914ce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Abenity_Sell_flow/AF_Abenity_E2E_TC_Sell_2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d72239b0-c0a4-46de-bb91-cceba3339fca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Abenity_Sell_flow/AF_Abenity_E2E_TC_Sell_3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>96a5848c-55ae-4460-a5b9-613ccdb377c8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Abenity_Sell_flow/AF_Abenity_E2E_TC_Sell_4</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>654caeae-b8e8-4fd9-bb68-36e2d5192620</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Abenity_Sell_flow/AF_Abenity_E2E_TC_Sell_5</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>786f8d1d-957c-4517-b8ca-bb429bd057d0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Abenity_Sell_flow/AF_Abenity_E2E_TC_Sell_6</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fa49ba61-73ce-48ab-9eed-697ed78e46ba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Abenity_Sell_flow/AF_Abenity_E2E_TC_Sell_7</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c8b01a9d-2f2d-4964-bfda-92a3505c332a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Abenity_Sell_flow/AF_Abenity_E2E_TC_Sell_8</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ebfc2b2b-6517-4ca5-b9b8-7af4b8d978e9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Abenity_Sell_flow/AF_Abenity_E2E_TC_Sell_9</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5ff5fea5-d893-44c3-9234-2c6f2d4911da</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Shyft_Both_flow/AF_Shyft_E2E_TC_Both_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>20ef4c6c-de90-4eee-8ef4-3270912b5c49</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Shyft_Both_flow/AF_Shyft_E2E_TC_Both_2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f847446d-1677-4730-9bac-f99245b3f392</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Shyft_Both_flow/AF_Shyft_E2E_TC_Both_3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9963e4ff-9b76-418b-8109-4739175a8adf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Shyft_Both_flow/AF_Shyft_E2E_TC_Both_4</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6c0f42ba-b4f6-4273-9ac3-a7d9d5db8ebf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Shyft_Both_flow/AF_Shyft_E2E_TC_Both_5</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6e991851-dbce-43f0-8ef7-07d45e587394</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Shyft_Both_flow/AF_Shyft_E2E_TC_Both_6</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>048ac661-9c95-4b01-816b-12e3cb6a23ac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Shyft_Both_flow/AF_Shyft_E2E_TC_Both_7</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2535af50-38f8-4f80-b321-a775cf066fdc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Shyft_Both_flow/AF_Shyft_E2E_TC_Both_8</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>10948f01-bb72-4e79-aa21-174d6ca720c5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Shyft_Both_flow/AF_Shyft_E2E_TC_Both_9</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>26c19ac5-78e6-4efb-89ea-d5823ec2facd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Shyft_Buy_flow/AF_Shyft_E2E_TC_Buy_1</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ca5d03ee-19cd-4449-9ab0-316d5ed5e8ce</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>ff323722-ba20-473f-96dd-2813cc03b4d7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Shyft_Buy_flow/AF_Shyft_E2E_TC_Buy_2</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>926af61c-9eb9-4f4b-b4c4-14bdc1624778</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>da067559-a171-4685-8248-90974a922c0b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Shyft_Buy_flow/AF_Shyft_E2E_TC_Buy_3</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7b885c2a-b6aa-4dd9-9a81-dee4418c96ff</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>7e8d87d5-54c5-4b7a-9808-267d66b4c549</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Shyft_Buy_flow/AF_Shyft_E2E_TC_Buy_4</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>5d2828f9-f1f5-4b5d-927e-8cd9d3282648</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>673a30e1-a913-4d81-a84d-d8c1c05822a8</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>74f2831c-1708-4349-bdfc-4f8e985800aa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Shyft_Buy_flow/AF_Shyft_E2E_TC_Buy_5</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>dc8e017c-c60a-488e-b1fd-5c73f9741815</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7a07a399-31e1-4254-85f3-746423863c48</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>a6dd5551-c2a6-467c-af02-b55808637a7a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Shyft_Buy_flow/AF_Shyft_E2E_TC_Buy_6</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>14e21e7e-50cb-47f9-bec3-bdced214f13d</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>800f253c-fffd-445c-b238-b1f8f463255f</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>cd85764a-dfdf-4cf1-b276-86995ebad360</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Shyft_Buy_flow/AF_Shyft_E2E_TC_Buy_7</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d613397e-2578-468c-9755-0d82e51bea8a</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>e6fbade8-3d55-47ac-9155-316d2f83a6e4</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>27df0f83-0b20-4987-8848-d11873291b06</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Shyft_Buy_flow/AF_Shyft_E2E_TC_Buy_8</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>efd318a8-1e16-4b58-958f-f0fd704dbe12</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>a64d27ec-123a-4029-af66-e0cf2d14bf10</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Shyft_Buy_flow/AF_Shyft_E2E_TC_Buy_9</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f10a87af-812e-4a49-805f-24cba09c0ea6</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>01fad306-3538-460e-960c-6e4e1dee50e0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Shyft_Sell_flow/AF_Shyft_E2E_TC_Sell_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fe981c00-5bf0-488e-b430-b19978ba37ad</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Shyft_Sell_flow/AF_Shyft_E2E_TC_Sell_2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a4c41cee-ab02-434c-8bee-d06b8916c6ba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Shyft_Sell_flow/AF_Shyft_E2E_TC_Sell_3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6e05f52d-0a52-4e12-b6cd-0072a77c06d0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Shyft_Sell_flow/AF_Shyft_E2E_TC_Sell_4</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4a7f13ba-ea6a-4b51-ae36-871584e57e04</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Shyft_Sell_flow/AF_Shyft_E2E_TC_Sell_5</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>199deb7d-862c-4acb-ae27-cecbb4f4f675</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Shyft_Sell_flow/AF_Shyft_E2E_TC_Sell_6</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>126e3763-8f6d-4fdf-8e69-5454d7cadc35</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Shyft_Sell_flow/AF_Shyft_E2E_TC_Sell_7</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>172a3384-5b28-4099-b092-65f12a2dc368</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Shyft_Sell_flow/AF_Shyft_E2E_TC_Sell_8</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>abec220f-c32f-44b5-8901-d6e4b97499e5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Shyft_Sell_flow/AF_Shyft_E2E_TC_Sell_9</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>81be300c-36ab-4410-82f5-29dbebe12bbf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Epp_Both_flow/AF_Epp_E2E_TC_Both_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d91a2e5b-6463-45ba-aac3-9c733afff667</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Epp_Both_flow/AF_Epp_E2E_TC_Both_2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1861245c-abdb-4f04-acce-6e71cd8bda06</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Epp_Both_flow/AF_Epp_E2E_TC_Both_3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4938a754-51c4-4132-9a9a-caf495f0b1bb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Epp_Both_flow/AF_Epp_E2E_TC_Both_4</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bfac7fec-ae72-435c-9381-946ba2062fcd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Epp_Both_flow/AF_Epp_E2E_TC_Both_5</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d710e56e-adb9-4bce-88c0-77a317b07801</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Epp_Both_flow/AF_Epp_E2E_TC_Both_6</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>814c1a58-6e40-4837-abea-28f2cd3d24fe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Epp_Both_flow/AF_Epp_E2E_TC_Both_7</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>222ffd22-4d17-4f26-a833-27aebdabe47f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Epp_Both_flow/AF_Epp_E2E_TC_Both_8</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0981449c-60b1-49cc-b230-a574999fc8d1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Epp_Both_flow/AF_Epp_E2E_TC_Both_9</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>32754ad9-c329-416f-ac83-59efbea9a3c3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Epp_Buy_flow/AF_Epp_E2E_TC_Buy_1</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>1a956130-ada8-47b1-b110-9b40a4603937</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>08b23a77-be6c-4a68-ae70-867752a51e77</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Epp_Buy_flow/AF_Epp_E2E_TC_Buy_2</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>94f1eced-bfc8-47d1-87de-351de4bde186</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>0f1fa85d-b3bd-41a5-ba71-493801fe99ab</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Epp_Buy_flow/AF_Epp_E2E_TC_Buy_3</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>aeb9b1c3-aad8-4855-801e-0b4d1c90f8e5</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>90bbb0c6-782d-448d-8955-f094237e1f06</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Epp_Buy_flow/AF_Epp_E2E_TC_Buy_4</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>9eccffad-83fe-479f-abec-d9c8dc0d24e6</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>52dceb1c-22bb-4f54-ad52-fbfdf536ecb6</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>ad9f3e90-4a87-46a4-b2bd-4ced0b84fb6d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Epp_Buy_flow/AF_Epp_E2E_TC_Buy_5</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ecc90ae8-4ae6-444a-953c-325fe7137786</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ec80d425-4c78-420d-a5aa-435c448acff2</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>b5fa1632-00db-4529-aaaa-1a95c8c41047</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Epp_Buy_flow/AF_Epp_E2E_TC_Buy_6</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>16d0d89e-0807-4132-a52d-326ff81181eb</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>80b7d9dd-7812-41b5-ae01-3a6ef800c6f4</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>cb4ec449-3f70-4664-9791-c530afa0d150</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Epp_Buy_flow/AF_Epp_E2E_TC_Buy_7</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>e3f58f0d-d1d4-447b-90dc-0f4757053e14</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>76079858-9ea9-4d5b-9159-027c5696e881</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>51fedb48-fb38-4c14-bd40-28372897edf2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Epp_Buy_flow/AF_Epp_E2E_TC_Buy_8</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>373de4dd-be16-40b5-97c3-0cb07a421cbb</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>88e0caf6-b1c4-49bf-acd3-a3dff27ef0cd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Epp_Buy_flow/AF_Epp_E2E_TC_Buy_9</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>95f7ca3b-36d7-406f-851e-8f117c8c63c1</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>30fa996e-dad9-429c-bbbb-786d1608ddf0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Epp_Sell_flow/AF_Epp_E2E_TC_Sell_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0d392a02-6b64-4087-af39-7476b2773af3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Epp_Sell_flow/AF_Epp_E2E_TC_Sell_2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f1bd3f86-7f2e-4e2c-94c7-0760ed133d74</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Epp_Sell_flow/AF_Epp_E2E_TC_Sell_3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>295b7e58-c86a-423c-ac6d-bc09e0838537</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Epp_Sell_flow/AF_Epp_E2E_TC_Sell_4</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>08b20367-5d0d-4709-ad6f-d6b3ce05f963</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Epp_Sell_flow/AF_Epp_E2E_TC_Sell_5</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1ef797a5-bfcb-43f6-bc76-f04ef3f5d7ec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Epp_Sell_flow/AF_Epp_E2E_TC_Sell_6</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ec3b39fb-aff9-4655-8fbe-a43647c1bef5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Epp_Sell_flow/AF_Epp_E2E_TC_Sell_7</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>91f8d70e-5493-4de7-b9a5-c468c5e2e8cc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Epp_Sell_flow/AF_Epp_E2E_TC_Sell_8</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>11530a03-8427-4159-bc81-f6ab1cedbc88</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Epp_Sell_flow/AF_Epp_E2E_TC_Sell_9</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>78e3a269-217e-48e6-9094-42c114e5e7f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Passport_Unlimited_Both_flow/AF_PU_E2E_TC_Both_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>195b27c8-7a7a-430a-bdb7-acefccfedb29</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Passport_Unlimited_Both_flow/AF_PU_E2E_TC_Both_2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f9a7d088-7b6e-43f7-8555-d4f1d3854c56</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Passport_Unlimited_Both_flow/AF_PU_E2E_TC_Both_3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>47081247-5774-407b-9a0e-22475927dcb7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Passport_Unlimited_Both_flow/AF_PU_E2E_TC_Both_4</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9fb02da0-462d-40a0-a9ff-132ea4d087de</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Passport_Unlimited_Both_flow/AF_PU_E2E_TC_Both_5</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c42eba31-df6f-482d-82a3-c7219790f55d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Passport_Unlimited_Both_flow/AF_PU_E2E_TC_Both_6</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f7a40040-3d8f-49c8-94cc-2936d20393f6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Passport_Unlimited_Both_flow/AF_PU_E2E_TC_Both_7</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9f0bbae0-e1be-4edb-98e6-87ce9f667d4b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Passport_Unlimited_Both_flow/AF_PU_E2E_TC_Both_8</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>58ee8525-7f13-460d-9d6f-a2a33db867be</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Passport_Unlimited_Both_flow/AF_PU_E2E_TC_Both_9</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6525595d-d16f-45eb-b22e-bee8e2b776b7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Passport_Unlimited_Buy_flow/AF_PU_E2E_TC_Buy_1</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>6b9697f6-de11-4184-875d-284f94ad78f2</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>1c3d82d4-ead8-4280-88a1-c0d1b98c3e30</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Passport_Unlimited_Buy_flow/AF_PU_E2E_TC_Buy_2</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>9d8c1567-c36f-4095-ad07-5c6d86a8c90f</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>cfdbc1e8-6ea5-4d26-9551-cc34c89eb3c2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Passport_Unlimited_Buy_flow/AF_PU_E2E_TC_Buy_3</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>53cc59d1-bb67-475d-8ada-c27ae35f2a8d</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>04b2021c-f998-48cd-ba99-933cdebb484f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Passport_Unlimited_Buy_flow/AF_PU_E2E_TC_Buy_4</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>e3779744-2445-40ad-911e-c1d4cc2a0ff0</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>6d4551b9-8f56-484f-968c-3a9f538cc6b5</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>8152c817-ca8b-48de-b49e-695f86eba2ef</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Passport_Unlimited_Buy_flow/AF_PU_E2E_TC_Buy_5</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7711734f-809c-4670-9b6c-ca540e94122f</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c3824281-0afb-4c59-8ceb-6a5d9d308ead</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>7c57207d-0bdc-4441-b608-37eb15a70dca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Passport_Unlimited_Buy_flow/AF_PU_E2E_TC_Buy_6</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>0888e02a-3508-407c-a159-c195bd5b3b21</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>940c2f8d-d3d3-46f5-a985-8efc99704947</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>cd7a7f39-3cb0-4465-ad59-a9c4fb325b39</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Passport_Unlimited_Buy_flow/AF_PU_E2E_TC_Buy_7</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>928ddc8a-0a91-42a2-8918-f296050c7135</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>5b50294b-765b-44c5-9ba1-69839252c2b6</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>030ddd5e-9e0c-467e-b95e-94bf23c5acbc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Passport_Unlimited_Buy_flow/AF_PU_E2E_TC_Buy_8</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>4ec7e4de-3a3f-480a-9d74-3fdebffbed47</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>ad2b0f0a-3bb5-4f16-ab85-635c4e440ebe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Passport_Unlimited_Buy_flow/AF_PU_E2E_TC_Buy_9</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>cb40ea68-17ca-45c8-9576-4e1df6dac605</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>4976e071-36da-4da2-95fc-c5d62ee923b1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Passport_Unlimited_Sell_flow/AF_PU_E2E_TC_Sell_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8182b782-379e-4094-8143-a5595ce0e604</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Passport_Unlimited_Sell_flow/AF_PU_E2E_TC_Sell_2</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>64229305-7c55-4bb3-82d9-54e7c47bcc7b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Passport_Unlimited_Sell_flow/AF_PU_E2E_TC_Sell_3</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e169e4d6-1041-4872-954b-15ca5d50c89e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Passport_Unlimited_Sell_flow/AF_PU_E2E_TC_Sell_4</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>025711e0-688d-4a21-bad5-de046bc31d49</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Passport_Unlimited_Sell_flow/AF_PU_E2E_TC_Sell_5</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2d05c634-f088-4da0-a91f-5c16e3d43831</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Passport_Unlimited_Sell_flow/AF_PU_E2E_TC_Sell_6</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6db505bc-f95e-4c9c-adc3-d4372b087d14</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Passport_Unlimited_Sell_flow/AF_PU_E2E_TC_Sell_7</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f48c6b4b-2510-4694-bf31-9f22bb85966d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Passport_Unlimited_Sell_flow/AF_PU_E2E_TC_Sell_8</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3e6c2b1e-220d-4c2d-8fd4-4ed0c59d1ff6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Passport_Unlimited_Sell_flow/AF_PU_E2E_TC_Sell_9</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
