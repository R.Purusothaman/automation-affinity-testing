<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test_Suite_RLG_Sites_Sprint 38</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient>R.Purusothaman@Cartus.com;R.Purusothaman@mindtree.com;</mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>true</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>7e713dbf-902f-473a-80fd-1edd38a061d8</testSuiteGuid>
   <testCaseLink>
      <guid>5a33d622-9edb-4776-b493-fbc8e870b152</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_USBANK/AF_USBANK_Both</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bdf14492-57c3-4c69-810f-6cbc8f927e66</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_USBANK/AF_USBANK_Buy</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>c6fa4c25-9599-400d-84fc-e1e7258fe817</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>11e9e38f-22cb-4b27-843d-c1876501094c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_USBANK/AF_USBANK_Sell</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>15cddf36-73f4-4c83-b878-675c4e6d8522</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_AARP/AF_AARP_E2E_TC_Both_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d6d727c7-c024-40f7-89a6-1a2b0eec4de2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_AARP/AF_AARP_E2E_TC_Buy_1</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>530bcfd2-da4d-4b07-b2fa-b3a901eba205</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>5b49d73c-468c-49ce-a487-5aea6c4cb7c4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_AARP/AF_AARP_E2E_TC_Sell_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>890aad73-aa8a-41fd-a798-185de0d15936</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_American_Airlines/AF_American_Airlines_E2E_TC_Both_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8fc24a72-3ac7-4d1b-85f8-2d5ba8d7058f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_American_Airlines/AF_American_Airlines_E2E_TC_Buy_1</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>71aee803-2fe6-4e94-a129-936928d9a975</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>9e72576c-554b-4147-a0ca-5d21ec719fb6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_American_Airlines/AF_American_Airlines_E2E_TC_Sell_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d3d2f989-212e-4cf7-badc-071583119e4a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_ASSOCIA/AF_ASSOCIA_E2E_TC_Both_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ca107307-8237-4483-a448-15076c445088</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_ASSOCIA/AF_ASSOCIA_E2E_TC_Buy_1</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7d055a38-3a38-40c8-9d77-e1d40b4fa76f</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>48e68fc0-5744-4579-939e-6be4a1347852</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_ASSOCIA/AF_ASSOCIA_E2E_TC_Sell_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>92699b76-e129-4b37-8a0b-b5cfc0a65333</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Guaranteedrate/AF_Guaranteedrate_E2E_TC_Both_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f25425f4-6c5a-4146-b30b-c7a78c5d975e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Guaranteedrate/AF_Guaranteedrate_E2E_TC_Buy_1</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f9987490-6c00-4d72-9851-1173b1b443b0</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>99888884-9a12-4eef-89f5-047483946abe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Guaranteedrate/AF_Guaranteedrate_E2E_TC_Sell_1</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
