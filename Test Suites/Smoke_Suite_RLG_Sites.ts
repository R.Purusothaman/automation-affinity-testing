<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Smoke_Suite_RLG_Sites</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>6c6cb768-b0f0-4de3-98ba-e11fe15ff965</testSuiteGuid>
   <testCaseLink>
      <guid>dcb3412d-32c5-4765-b729-36e059addccf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_AARP_Both_flow/AF_AARP_E2E_TC_Both_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>16a1c02e-a080-444c-810f-08ed62980a11</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_AARP_Buy_flow/AF_AARP_E2E_TC_Buy_1</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>55d817f0-335b-41e9-8491-5034a42e60d2</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>c7e1be2a-6fc6-408e-a1e6-c7f987287653</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_AARP_Sell_flow/AF_AARP_E2E_TC_Sell_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>675f4d66-5b7b-44b8-85c5-93d9b9f45769</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Abenity_Both_flow/AF_Abenity_E2E_TC_Both_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b19f5f5a-b749-4ec6-824e-91bdfda860b0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Abenity_Buy_flow/AF_Abenity_E2E_TC_Buy_1</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>199dfe4d-bd6e-4d61-bd91-1fe611b20485</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>04d89551-2b51-44f7-a115-4e815e51d988</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Abenity_Sell_flow/AF_Abenity_E2E_TC_Sell_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7ad0a2f4-02a6-41e1-a43d-0af09c65e84c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_American_Airlines_Both_flow/AF_American_Airlines_E2E_TC_Both_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>54b7e760-5112-4f4d-b0b5-93cbea43e012</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_American_Airlines_Buy_flow/AF_American_Airlines_E2E_TC_Buy_1</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d3a71fa7-1a50-4111-b0ac-6636c28f2a67</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>a16a7e2a-27b0-46e4-95ba-836a72499cc0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_American_Airlines_Sell_flow/AF_American_Airlines_E2E_TC_Sell_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c5bcc737-31f2-4eb4-862e-31cda202a766</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Associa_Both_flow/AF_Associa_E2E_TC_Both_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eaa86c91-69ff-4c71-8374-96e928bbbc80</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Associa_Buy_flow/AF_Associa_E2E_TC_Buy_1</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ad2fd208-e010-4f58-b7d1-0d84aef83bc9</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>8a76fcad-4b2c-4a8a-8116-60d0f0814e95</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Associa_Sell_flow/AF_Associa_E2E_TC_Sell_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7388c33e-c2ac-4e61-8e41-3e214817871c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_BenefitHub_Both_flow/AF_BenefitHub_E2E_TC_Both_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f3b605d1-285f-4d73-b095-10be196f6b88</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_BenefitHub_Buy_flow/AF_BenefitHub_E2E_TC_Buy_1</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ffbae0e8-a86d-43bc-b4f1-a104b1369c70</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>fd72fd99-3706-48cf-bc7a-2b6241f684d7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_BenefitHub_Sell_flow/AF_BenefitHub_E2E_TC_Sell_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d52ca158-b22d-4faf-ab4c-2ffb868015a6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Epp_Buy_flow/AF_Epp_E2E_TC_Buy_1</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>1a956130-ada8-47b1-b110-9b40a4603937</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>1f7d3092-f742-45b8-a06c-9c9ea2dd1ede</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Epp_Both_flow/AF_Epp_E2E_TC_Both_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>10391101-16b8-4e37-b04e-048ce574c1aa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Epp_Sell_flow/AF_Epp_E2E_TC_Sell_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d0a3ecf6-a12d-49bf-8b88-e40cbde824a8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Guaranteedrate_Both_flow/AF_Guaranteedrate_E2E_TC_Both_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7d7bbe00-bbe7-4f0b-8b2d-5937bcceed6f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Guaranteedrate_Buy_flow/AF_Guaranteedrate_E2E_TC_Buy_1</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>52a92b35-622d-4200-939e-78f9371ab46c</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>dc427bd2-7d49-4663-b4e9-cf3aee3e4b69</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Guaranteedrate_Sell_flow/AF_Guaranteedrate_E2E_TC_Sell_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0c9d9af5-d500-490f-a000-e0a547a433d1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Passport_Unlimited_Both_flow/AF_PU_E2E_TC_Both_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e332fbee-ab5e-4092-be56-b2b8b0954944</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Passport_Unlimited_Buy_flow/AF_PU_E2E_TC_Buy_1</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>6b9697f6-de11-4184-875d-284f94ad78f2</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>2a354e9d-ead0-49a4-ae52-d4030ad1e063</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Passport_Unlimited_Sell_flow/AF_PU_E2E_TC_Sell_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e8f3f9f9-4d8e-47d6-a58a-3b66a2373757</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Realogy_Leads_Groups_Both_flow/AF_RLG_E2E_TC_Both_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>128f0cd2-6c1c-45ae-bb17-e1eb35d26be1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Realogy_Leads_Groups_Buy_flow/AF_RLG_E2E_TC_Buy_1</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>02a1b14b-2117-4d34-9b2c-ef4b0f6dc214</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>1f027c5d-ccae-4302-b909-3463f6901042</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Realogy_Leads_Groups_Sell_flow/AF_RLG_E2E_TC_Sell_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>deefd8f4-538f-4e51-b19a-b5109bd9c123</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Shyft_Both_flow/AF_Shyft_E2E_TC_Both_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>00d30ede-0183-4ccb-b193-c8804903e5b8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Shyft_Buy_flow/AF_Shyft_E2E_TC_Buy_1</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ca5d03ee-19cd-4449-9ab0-316d5ed5e8ce</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>a0a47001-4a5d-4d54-9534-25b0f395de05</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_Shyft_Sell_flow/AF_Shyft_E2E_TC_Sell_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>58693f87-fb0b-4468-8930-5077fa225045</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_USBANK_Both_flow/AF_USBANK_E2E_TC_Both_1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e0dd032d-15fb-4974-8b9d-adca251bf449</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_USBANK_Buy_flow/AF_USBANK_E2E_TC_Buy_1</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>b20921ea-5690-4d86-b681-29fd536451c8</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>a06cf717-f400-4364-b23d-eafc44879b3d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Create Referral Page/AF_USBANK_Sell_flow/AF_USBANK_E2E_TC_Sell_1</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
